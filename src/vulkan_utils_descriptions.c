/***********************************************************************/
/* Name        : vulkan_utils_descriptions.c                           */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#include <vulkan/vulkan.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "c_utils.h"
#include "vulkan_utils.h"

void vkuDescribeSampleCountFlags(const VkSampleCountFlags sampleCountFlags)
{
	bool support = false;
	int sampleCountBit[7] = { 0 };
	int i = 0;

	if((sampleCountFlags & VK_SAMPLE_COUNT_1_BIT) == VK_SAMPLE_COUNT_1_BIT)
		sampleCountBit[i++] = 1;
	if((sampleCountFlags & VK_SAMPLE_COUNT_2_BIT) == VK_SAMPLE_COUNT_2_BIT)
		sampleCountBit[i++] = 2;
	if((sampleCountFlags & VK_SAMPLE_COUNT_4_BIT) == VK_SAMPLE_COUNT_4_BIT)
		sampleCountBit[i++] = 4;
	if((sampleCountFlags & VK_SAMPLE_COUNT_8_BIT) == VK_SAMPLE_COUNT_8_BIT)
		sampleCountBit[i++] = 8;
	if((sampleCountFlags & VK_SAMPLE_COUNT_16_BIT) == VK_SAMPLE_COUNT_16_BIT)
		sampleCountBit[i++] = 16;
	if((sampleCountFlags & VK_SAMPLE_COUNT_32_BIT) == VK_SAMPLE_COUNT_32_BIT)
		sampleCountBit[i++] = 32;
	if((sampleCountFlags & VK_SAMPLE_COUNT_64_BIT) == VK_SAMPLE_COUNT_64_BIT)
		sampleCountBit[i++] = 64;

	char mask[(7 * (32 + 2) + 1)] = { 0 };
	for(i = 0; i < 7; i++)
	{
		if(sampleCountBit[i] > 0)
		{
			if(!support)
			{
				sprintf(&(mask[strlen(mask)]), "%i", sampleCountBit[i]);
				support = true;
			}
			else
				sprintf(&(mask[strlen(mask)]), ", %i", sampleCountBit[i]);
		}
	}
	mask[strlen(mask)] = '\0';
	fprintf(stdout, "%s%s%s\n", "Can handle an image with {", mask, "} samples per pixel.");
}

void vkuDescribeMemoryPropertyFlags(const VkMemoryPropertyFlags memoryPropertyFlags)
{
	char mask[192] = { 0 };

	if((memoryPropertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
		sprintf(&(mask[strlen(mask)]), "%s", "VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT");
	if((memoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT");
	if((memoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) == VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_MEMORY_PROPERTY_HOST_COHERENT_BIT");
	if((memoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) == VK_MEMORY_PROPERTY_HOST_CACHED_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_MEMORY_PROPERTY_HOST_CACHED_BIT");
	if((memoryPropertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT) == VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT");
	mask[strlen(mask)] = '\0';

	if(strlen(mask)<=0)
		fprintf(stdout, "NONE\n");
	else
		fprintf(stdout, "%s\n", mask);
}

void vkuDescribeQueueFlags(const VkQueueFlags queueFlags)
{
	char mask[99] = { 0 };

	if((queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT)
		sprintf(&(mask[strlen(mask)]), "%s", "VK_QUEUE_GRAPHICS_BIT");
	if((queueFlags & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_QUEUE_COMPUTE_BIT");
	if((queueFlags & VK_QUEUE_TRANSFER_BIT) == VK_QUEUE_TRANSFER_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_QUEUE_TRANSFER_BIT");
	if((queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) == VK_QUEUE_SPARSE_BINDING_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_QUEUE_SPARSE_BINDING_BIT");
	mask[strlen(mask)] = '\0';

	if(strlen(mask)<=0)
		fprintf(stdout, "NONE\n");
	else
		fprintf(stdout, "%s\n", mask);
}

void vkuDescribeMemoryHeapFlags(const VkMemoryHeapFlags memoryHeapFlags)
{
	char mask[32] = { 0 };

	if((memoryHeapFlags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) == VK_MEMORY_HEAP_DEVICE_LOCAL_BIT)
		sprintf(&(mask[strlen(mask)]), "%s", "VK_MEMORY_HEAP_DEVICE_LOCAL_BIT");
	mask[strlen(mask)] = '\0';

	if(strlen(mask)<=0)
		fprintf(stdout, "NONE\n");
	else
		fprintf(stdout, "%s\n", mask);
}

void vkuDescribePhysicalDeviceLimitsProperties(const char *physicalDeviceLimitsName, const VkPhysicalDeviceLimits physicalDeviceLimits)
{
	fprintf(stdout, "%s.maxImageDimension1D = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxImageDimension1D);
	fprintf(stdout, "%s.maxImageDimension2D = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxImageDimension2D);
	fprintf(stdout, "%s.maxImageDimension3D = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxImageDimension3D);
	fprintf(stdout, "%s.maxImageDimensionCube = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxImageDimensionCube);
	fprintf(stdout, "%s.maxImageArrayLayers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxImageArrayLayers);
	fprintf(stdout, "%s.maxTexelBufferElements = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTexelBufferElements);
	fprintf(stdout, "%s.maxUniformBufferRange = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxUniformBufferRange);
	fprintf(stdout, "%s.maxStorageBufferRange = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxStorageBufferRange);
	fprintf(stdout, "%s.maxPushConstantsSize = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPushConstantsSize);
	fprintf(stdout, "%s.maxMemoryAllocationCount = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxMemoryAllocationCount);
	fprintf(stdout, "%s.maxSamplerAllocationCount = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxSamplerAllocationCount);
	fprintf(stdout, "%s.bufferImageGranularity = %I64u\n", physicalDeviceLimitsName, physicalDeviceLimits.bufferImageGranularity);
	fprintf(stdout, "%s.sparseAddressSpaceSize = %I64u\n", physicalDeviceLimitsName, physicalDeviceLimits.sparseAddressSpaceSize);
	fprintf(stdout, "%s.maxBoundDescriptorSets = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxBoundDescriptorSets);
	fprintf(stdout, "%s.maxPerStageDescriptorSamplers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorSamplers);
	fprintf(stdout, "%s.maxPerStageDescriptorUniformBuffers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorUniformBuffers);
	fprintf(stdout, "%s.maxPerStageDescriptorStorageBuffers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorStorageBuffers);
	fprintf(stdout, "%s.maxPerStageDescriptorSampledImages = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorSampledImages);
	fprintf(stdout, "%s.maxPerStageDescriptorStorageImages = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorStorageImages);
	fprintf(stdout, "%s.maxPerStageDescriptorInputAttachments = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorInputAttachments);
	fprintf(stdout, "%s.maxPerStageResources = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageResources);
	fprintf(stdout, "%s.maxDescriptorSetSamplers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetSamplers);
	fprintf(stdout, "%s.maxDescriptorSetUniformBuffers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetUniformBuffers);
	fprintf(stdout, "%s.maxDescriptorSetUniformBuffersDynamic = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetUniformBuffersDynamic);
	fprintf(stdout, "%s.maxDescriptorSetStorageBuffers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetStorageBuffers);
	fprintf(stdout, "%s.maxDescriptorSetStorageBuffersDynamic = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetStorageBuffersDynamic);
	fprintf(stdout, "%s.maxDescriptorSetSampledImages = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetSampledImages);
	fprintf(stdout, "%s.maxDescriptorSetStorageImages = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetStorageImages);
	fprintf(stdout, "%s.maxDescriptorSetInputAttachments = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetInputAttachments);
	fprintf(stdout, "%s.maxVertexInputAttributes = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxVertexInputAttributes);
	fprintf(stdout, "%s.maxVertexInputBindings = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxVertexInputBindings);
	fprintf(stdout, "%s.maxVertexInputAttributeOffset = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxVertexInputAttributeOffset);
	fprintf(stdout, "%s.maxVertexInputBindingStride = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxVertexInputBindingStride);
	fprintf(stdout, "%s.maxVertexOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxVertexOutputComponents);
	fprintf(stdout, "%s.maxTessellationGenerationLevel = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationGenerationLevel);
	fprintf(stdout, "%s.maxTessellationPatchSize = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationPatchSize);
	fprintf(stdout, "%s.maxTessellationControlPerVertexInputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationControlPerVertexInputComponents);
	fprintf(stdout, "%s.maxTessellationControlPerVertexOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationControlPerVertexOutputComponents);
	fprintf(stdout, "%s.maxTessellationControlPerPatchOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationControlPerPatchOutputComponents);
	fprintf(stdout, "%s.maxTessellationControlTotalOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationControlTotalOutputComponents);
	fprintf(stdout, "%s.maxTessellationEvaluationInputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationEvaluationInputComponents);
	fprintf(stdout, "%s.maxTessellationEvaluationOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationEvaluationOutputComponents);
	fprintf(stdout, "%s.maxGeometryShaderInvocations = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxGeometryShaderInvocations);
	fprintf(stdout, "%s.maxGeometryInputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxGeometryInputComponents);
	fprintf(stdout, "%s.maxGeometryOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxGeometryOutputComponents);
	fprintf(stdout, "%s.maxGeometryOutputVertices = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxGeometryOutputVertices);
	fprintf(stdout, "%s.maxGeometryTotalOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxGeometryTotalOutputComponents);
	fprintf(stdout, "%s.maxFragmentInputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFragmentInputComponents);
	fprintf(stdout, "%s.maxFragmentOutputAttachments = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFragmentOutputAttachments);
	fprintf(stdout, "%s.maxFragmentDualSrcAttachments = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFragmentDualSrcAttachments);
	fprintf(stdout, "%s.maxFragmentCombinedOutputResources = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFragmentCombinedOutputResources);
	fprintf(stdout, "%s.maxComputeSharedMemorySize = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeSharedMemorySize);
	fprintf(stdout, "%s.maxComputeWorkGroupCount[0] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupCount[0]);
	fprintf(stdout, "%s.maxComputeWorkGroupCount[1] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupCount[1]);
	fprintf(stdout, "%s.maxComputeWorkGroupCount[2] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupCount[2]);
	fprintf(stdout, "%s.maxComputeWorkGroupInvocations = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupInvocations);
	fprintf(stdout, "%s.maxComputeWorkGroupSize[0] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupSize[0]);
	fprintf(stdout, "%s.maxComputeWorkGroupSize[1] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupSize[1]);
	fprintf(stdout, "%s.maxComputeWorkGroupSize[2] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupSize[2]);
	fprintf(stdout, "%s.subPixelPrecisionBits = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.subPixelPrecisionBits);
	fprintf(stdout, "%s.subTexelPrecisionBits = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.subTexelPrecisionBits);
	fprintf(stdout, "%s.mipmapPrecisionBits = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.mipmapPrecisionBits);
	fprintf(stdout, "%s.maxDrawIndexedIndexValue = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDrawIndexedIndexValue);
	fprintf(stdout, "%s.maxDrawIndirectCount = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDrawIndirectCount);
	fprintf(stdout, "%s.maxSamplerLodBias = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.maxSamplerLodBias);
	fprintf(stdout, "%s.maxSamplerAnisotropy = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.maxSamplerAnisotropy);
	fprintf(stdout, "%s.maxViewports = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxViewports);
	fprintf(stdout, "%s.maxViewportDimensions[0] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxViewportDimensions[0]);
	fprintf(stdout, "%s.maxViewportDimensions[1] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxViewportDimensions[1]);
	fprintf(stdout, "%s.viewportBoundsRange[0] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.viewportBoundsRange[0]);
	fprintf(stdout, "%s.viewportBoundsRange[1] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.viewportBoundsRange[1]);
	fprintf(stdout, "%s.viewportSubPixelBits = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.viewportSubPixelBits);
	fprintf(stdout, "%s.minMemoryMapAlignment = %I64u\n", physicalDeviceLimitsName, physicalDeviceLimits.minMemoryMapAlignment);
	fprintf(stdout, "%s.minTexelBufferOffsetAlignment = %I64u\n", physicalDeviceLimitsName, physicalDeviceLimits.minTexelBufferOffsetAlignment);
	fprintf(stdout, "%s.minUniformBufferOffsetAlignment = %I64u\n", physicalDeviceLimitsName, physicalDeviceLimits.minUniformBufferOffsetAlignment);
	fprintf(stdout, "%s.minStorageBufferOffsetAlignment = %I64u\n", physicalDeviceLimitsName, physicalDeviceLimits.minStorageBufferOffsetAlignment);
	fprintf(stdout, "%s.minTexelOffset = %i\n", physicalDeviceLimitsName, physicalDeviceLimits.minTexelOffset);
	fprintf(stdout, "%s.maxTexelOffset = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTexelOffset);
	fprintf(stdout, "%s.minTexelGatherOffset = %i\n", physicalDeviceLimitsName, physicalDeviceLimits.minTexelGatherOffset);
	fprintf(stdout, "%s.maxTexelGatherOffset = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTexelGatherOffset);
	fprintf(stdout, "%s.minInterpolationOffset = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.viewportBoundsRange[1]);
	fprintf(stdout, "%s.maxInterpolationOffset = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.viewportBoundsRange[1]);
	fprintf(stdout, "%s.subPixelInterpolationOffsetBits = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.subPixelInterpolationOffsetBits);
	fprintf(stdout, "%s.maxFramebufferWidth = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFramebufferWidth);
	fprintf(stdout, "%s.maxFramebufferHeight = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFramebufferHeight);
	fprintf(stdout, "%s.maxFramebufferLayers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFramebufferLayers);
	fprintf(stdout, "%s.framebufferColorSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.framebufferColorSampleCounts);
	vkuDescribeSampleCountFlags(physicalDeviceLimits.framebufferColorSampleCounts);
	fprintf(stdout, "%s.framebufferDepthSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.framebufferDepthSampleCounts);
	vkuDescribeSampleCountFlags(physicalDeviceLimits.framebufferDepthSampleCounts);
	fprintf(stdout, "%s.framebufferStencilSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.framebufferStencilSampleCounts);
	vkuDescribeSampleCountFlags(physicalDeviceLimits.framebufferStencilSampleCounts);
	fprintf(stdout, "%s.framebufferNoAttachmentsSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.framebufferNoAttachmentsSampleCounts);
	vkuDescribeSampleCountFlags(physicalDeviceLimits.framebufferNoAttachmentsSampleCounts);
	fprintf(stdout, "%s.maxColorAttachments = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxColorAttachments);
	fprintf(stdout, "%s.sampledImageColorSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.sampledImageColorSampleCounts);
	vkuDescribeSampleCountFlags(physicalDeviceLimits.sampledImageColorSampleCounts);
	fprintf(stdout, "%s.sampledImageIntegerSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.sampledImageIntegerSampleCounts);
	vkuDescribeSampleCountFlags(physicalDeviceLimits.sampledImageIntegerSampleCounts);
	fprintf(stdout, "%s.sampledImageDepthSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.sampledImageDepthSampleCounts);
	vkuDescribeSampleCountFlags(physicalDeviceLimits.sampledImageDepthSampleCounts);
	fprintf(stdout, "%s.sampledImageStencilSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.sampledImageStencilSampleCounts);
	vkuDescribeSampleCountFlags(physicalDeviceLimits.sampledImageStencilSampleCounts);
	fprintf(stdout, "%s.storageImageSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.storageImageSampleCounts);
	vkuDescribeSampleCountFlags(physicalDeviceLimits.storageImageSampleCounts);
	fprintf(stdout, "%s.maxSampleMaskWords = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxSampleMaskWords);
	fprintf(stdout, "%s.timestampComputeAndGraphics = %s\n", physicalDeviceLimitsName, ((physicalDeviceLimits.timestampComputeAndGraphics == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.timestampPeriod = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.timestampPeriod);
	fprintf(stdout, "%s.maxClipDistances = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxClipDistances);
	fprintf(stdout, "%s.maxCullDistances = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxCullDistances);
	fprintf(stdout, "%s.maxCombinedClipAndCullDistances = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxCombinedClipAndCullDistances);
	fprintf(stdout, "%s.discreteQueuePriorities = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.discreteQueuePriorities);
	fprintf(stdout, "%s.pointSizeRange[0] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.pointSizeRange[0]);
	fprintf(stdout, "%s.pointSizeRange[1] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.pointSizeRange[1]);
	fprintf(stdout, "%s.lineWidthRange[0] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.lineWidthRange[0]);
	fprintf(stdout, "%s.lineWidthRange[1] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.lineWidthRange[1]);
	fprintf(stdout, "%s.pointSizeGranularity = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.pointSizeGranularity);
	fprintf(stdout, "%s.lineWidthGranularity = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.lineWidthGranularity);
	fprintf(stdout, "%s.strictLines = %s\n", physicalDeviceLimitsName, ((physicalDeviceLimits.strictLines == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.standardSampleLocations = %s\n", physicalDeviceLimitsName, ((physicalDeviceLimits.standardSampleLocations == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.optimalBufferCopyOffsetAlignment = %I64u\n", physicalDeviceLimitsName, physicalDeviceLimits.optimalBufferCopyOffsetAlignment);
	fprintf(stdout, "%s.optimalBufferCopyRowPitchAlignment = %I64u\n", physicalDeviceLimitsName, physicalDeviceLimits.optimalBufferCopyRowPitchAlignment);
	fprintf(stdout, "%s.nonCoherentAtomSize = %I64u\n", physicalDeviceLimitsName, physicalDeviceLimits.nonCoherentAtomSize);
}

void vkuDescribePhysicalDeviceSparseProperties(const char *physicalDeviceSparsePropertiesName, const VkPhysicalDeviceSparseProperties physicalDeviceSparseProperties)
{
	fprintf(stdout, "%s.residencyStandard2DBlockShape            = %s\n", physicalDeviceSparsePropertiesName, ((physicalDeviceSparseProperties.residencyStandard2DBlockShape == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.residencyStandard2DMultisampleBlockShape = %s\n", physicalDeviceSparsePropertiesName, ((physicalDeviceSparseProperties.residencyStandard2DMultisampleBlockShape == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.residencyStandard3DBlockShape            = %s\n", physicalDeviceSparsePropertiesName, ((physicalDeviceSparseProperties.residencyStandard3DBlockShape == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.residencyAlignedMipSize                  = %s\n", physicalDeviceSparsePropertiesName, ((physicalDeviceSparseProperties.residencyAlignedMipSize == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.residencyNonResidentStrict               = %s\n", physicalDeviceSparsePropertiesName, ((physicalDeviceSparseProperties.residencyNonResidentStrict == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
}

void vkuDescribePhysicalDeviceProperties(const char *physicalDevicePropertiesName, const VkPhysicalDeviceProperties physicalDeviceProperties)
{
	fprintf(stdout, "%s.apiVersion = 0x%08X\n", physicalDevicePropertiesName, physicalDeviceProperties.apiVersion);
	fprintf(stdout, "%s.driverVersion = %u\n", physicalDevicePropertiesName, physicalDeviceProperties.driverVersion);
	fprintf(stdout, "%s.vendorID = %u\n", physicalDevicePropertiesName, physicalDeviceProperties.vendorID);
	fprintf(stdout, "%s.deviceID = %u\n", physicalDevicePropertiesName, physicalDeviceProperties.deviceID);
	fprintf(stdout, "%s.deviceType = %s\n", physicalDevicePropertiesName, vkuGetPhysicalDeviceTypeSignification(physicalDeviceProperties.deviceType));
	fprintf(stdout, "%s.deviceName = %s\n", physicalDevicePropertiesName, physicalDeviceProperties.deviceName);
	for(int i = 0; i < VK_UUID_SIZE; i++)
	{
		fprintf(stdout, "%s.pipelineCacheUUID[%2i] = 0x%02hX\n", physicalDevicePropertiesName, i, physicalDeviceProperties.pipelineCacheUUID[i]);
	}

	char *physicalDeviceLimitsName = (char*) malloc((strlen(physicalDevicePropertiesName) + 8) * sizeof(char));
	sprintf(physicalDeviceLimitsName, "%s.%s", physicalDevicePropertiesName, "limits");
	vkuDescribePhysicalDeviceLimitsProperties(physicalDeviceLimitsName, physicalDeviceProperties.limits);
	free(physicalDeviceLimitsName);

	char *physicalSparsePropertiesName = (char*) malloc((strlen(physicalDevicePropertiesName) + 18) * sizeof(char));
	sprintf(physicalSparsePropertiesName, "%s.%s", physicalDevicePropertiesName, "sparseProperties");
	vkuDescribePhysicalDeviceSparseProperties(physicalSparsePropertiesName, physicalDeviceProperties.sparseProperties);
	free(physicalSparsePropertiesName);
}

void vkuDescribePhysicalDeviceFeatures(const char *physicalDeviceFeaturesName, const VkPhysicalDeviceFeatures physicalDeviceFeatures)
{
	fprintf(stdout, "%s.robustBufferAccess =                      %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.robustBufferAccess == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.fullDrawIndexUint32 =                     %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.fullDrawIndexUint32 == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.imageCubeArray =                          %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.imageCubeArray == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.independentBlend =                        %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.independentBlend == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.geometryShader =                          %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.geometryShader == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.tessellationShader =                      %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.tessellationShader == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.sampleRateShading =                       %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sampleRateShading == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.dualSrcBlend =                            %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.dualSrcBlend == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.logicOp =                                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.logicOp == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.multiDrawIndirect =                       %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.multiDrawIndirect == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.drawIndirectFirstInstance =               %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.drawIndirectFirstInstance == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.depthClamp =                              %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.depthClamp == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.depthBiasClamp =                          %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.depthBiasClamp == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.fillModeNonSolid =                        %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.fillModeNonSolid == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.depthBounds =                             %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.depthBounds == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.wideLines =                               %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.wideLines == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.largePoints =                             %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.largePoints == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.alphaToOne =                              %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.alphaToOne == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.multiViewport =                           %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.multiViewport == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.samplerAnisotropy =                       %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.samplerAnisotropy == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.textureCompressionETC2 =                  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.textureCompressionETC2 == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.textureCompressionASTC_LDR =              %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.textureCompressionASTC_LDR == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.textureCompressionBC =                    %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.textureCompressionBC == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.occlusionQueryPrecise =                   %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.occlusionQueryPrecise == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.pipelineStatisticsQuery =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.pipelineStatisticsQuery == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.vertexPipelineStoresAndAtomics =          %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.vertexPipelineStoresAndAtomics == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.fragmentStoresAndAtomics =                %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.fragmentStoresAndAtomics == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderTessellationAndGeometryPointSize =  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderTessellationAndGeometryPointSize == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderImageGatherExtended =               %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderImageGatherExtended == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderStorageImageExtendedFormats =       %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageImageExtendedFormats == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderStorageImageMultisample =           %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageImageMultisample == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderStorageImageReadWithoutFormat =     %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageImageReadWithoutFormat == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderStorageImageWriteWithoutFormat =    %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageImageWriteWithoutFormat == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderUniformBufferArrayDynamicIndexing = %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderUniformBufferArrayDynamicIndexing == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderSampledImageArrayDynamicIndexing =  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderSampledImageArrayDynamicIndexing == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderStorageBufferArrayDynamicIndexing = %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageBufferArrayDynamicIndexing == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderStorageImageArrayDynamicIndexing =  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageImageArrayDynamicIndexing == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderClipDistance =                      %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderClipDistance == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderCullDistance =                      %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderCullDistance == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderFloat64 =                           %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderFloat64 == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderInt64 =                             %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderInt64 == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderInt16 =                             %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderInt16 == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderResourceResidency =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderResourceResidency == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.shaderResourceMinLod =                    %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderResourceMinLod == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.sparseBinding =                           %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseBinding == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.sparseResidencyBuffer =                   %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidencyBuffer == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.sparseResidencyImage2D =                  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidencyImage2D == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.sparseResidencyImage3D =                  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidencyImage3D == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.sparseResidency2Samples =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidency2Samples == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.sparseResidency4Samples =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidency4Samples == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.sparseResidency8Samples =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidency8Samples == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.sparseResidency16Samples =                %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidency16Samples == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.sparseResidencyAliased =                  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidencyAliased == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.variableMultisampleRate =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.variableMultisampleRate == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	fprintf(stdout, "%s.inheritedQueries =                        %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.inheritedQueries == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
}

void vkuDescribeMemoryType(const char *memoryTypeName, const VkMemoryType memoryType)
{
	/*if((memoryType.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
		fprintf(stdout, "%s.propertyFlags & %s\n", memoryTypeName, vkuGetMemoryPropertyFlagBitsSignification(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT));
	if((memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
		fprintf(stdout, "%s.propertyFlags & %s\n", memoryTypeName, vkuGetMemoryPropertyFlagBitsSignification(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT));
	if((memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) == VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
		fprintf(stdout, "%s.propertyFlags & %s\n", memoryTypeName, vkuGetMemoryPropertyFlagBitsSignification(VK_MEMORY_PROPERTY_HOST_COHERENT_BIT));
	if((memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) == VK_MEMORY_PROPERTY_HOST_CACHED_BIT)
		fprintf(stdout, "%s.propertyFlags & %s\n", memoryTypeName, vkuGetMemoryPropertyFlagBitsSignification(VK_MEMORY_PROPERTY_HOST_CACHED_BIT));
	if((memoryType.propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT) == VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT)
		fprintf(stdout, "%s.propertyFlags & %s\n", memoryTypeName, vkuGetMemoryPropertyFlagBitsSignification(VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT));*/
	fprintf(stdout, "%s.propertyFlags = ", memoryTypeName);
	vkuDescribeMemoryPropertyFlags(memoryType.propertyFlags);
	fprintf(stdout, "%s.heapIndex = %u\n", memoryTypeName, memoryType.heapIndex);

}

void vkuDescribeMemoryHeap(const char *memoryHeapName, const VkMemoryHeap memoryHeap)
{
	/*if((memoryHeap.flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) == VK_MEMORY_HEAP_DEVICE_LOCAL_BIT)
		fprintf(stdout, "%s.flags & %s\n", memoryHeapName, vkuGetMemoryHeapFlagBitsSignification(VK_MEMORY_HEAP_DEVICE_LOCAL_BIT));*/
	fprintf(stdout, "%s.flags = ", memoryHeapName);
	vkuDescribeMemoryHeapFlags(memoryHeap.flags);
	fprintf(stdout, "%s.size = %I64u\n", memoryHeapName, memoryHeap.size);
}

void vkuDescribePhysicalDeviceMemoryProperties(const char *physicalDeviceMemoryPropertiesName, const VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties)
{
	//VkMemoryType memoryTypes[VK_MAX_MEMORY_TYPES] = physicalDeviceMemoryProperties.memoryTypes;
	//VkMemoryHeap memoryHeaps[VK_MAX_MEMORY_HEAPS] = physicalDeviceMemoryProperties.memoryHeaps;

	fprintf(stdout, "%s.memoryTypeCount = %u\n", physicalDeviceMemoryPropertiesName, physicalDeviceMemoryProperties.memoryTypeCount);
	unsigned int i = 0;
	char *physicalDeviceMemoryPropertiesMemoryTypeName = (char*) malloc((strlen(physicalDeviceMemoryPropertiesName) + 47) * sizeof(char));
	for(i = 0; i < physicalDeviceMemoryProperties.memoryTypeCount; i++)
	{
		sprintf(physicalDeviceMemoryPropertiesMemoryTypeName, "%s.%s[%i]", physicalDeviceMemoryPropertiesName, "memory_type", i);
		vkuDescribeMemoryType(physicalDeviceMemoryPropertiesMemoryTypeName, physicalDeviceMemoryProperties.memoryTypes[i]);
	}
	free(physicalDeviceMemoryPropertiesMemoryTypeName);
	fprintf(stdout, "%s.memoryHeapCount = %u\n", physicalDeviceMemoryPropertiesName, physicalDeviceMemoryProperties.memoryHeapCount);
	char *physicalDeviceMemoryPropertiesMemoryHeapName = (char*) malloc((strlen(physicalDeviceMemoryPropertiesName) + 47) * sizeof(char));
	for(i = 0; i < physicalDeviceMemoryProperties.memoryHeapCount; i++)
	{
		sprintf(physicalDeviceMemoryPropertiesMemoryHeapName, "%s.%s[%i]", physicalDeviceMemoryPropertiesName, "memory_heap", i);
		vkuDescribeMemoryHeap(physicalDeviceMemoryPropertiesMemoryHeapName, physicalDeviceMemoryProperties.memoryHeaps[i]);
	}
	free(physicalDeviceMemoryPropertiesMemoryHeapName);
}

void vkuDescribeExtent3D(const char *extent3DName, VkExtent3D extent3D)
{
	fprintf(stdout, "%s.width =  %u\n", extent3DName, extent3D.width);
	fprintf(stdout, "%s.height = %u\n", extent3DName, extent3D.height);
	fprintf(stdout, "%s.depth =  %u\n", extent3DName, extent3D.depth);
}

void vkuDescribeQueueFamilyProperties(const char *queueFamilyPropertiesName, const VkQueueFamilyProperties queueFamilyProperties)
{
	fprintf(stdout, "%s.queueFlags = ", queueFamilyPropertiesName);
	vkuDescribeQueueFlags(queueFamilyProperties.queueFlags);
	fprintf(stdout, "%s.queueCount = %u\n", queueFamilyPropertiesName, queueFamilyProperties.queueCount);
	fprintf(stdout, "%s.timestampValidBits = %u\n", queueFamilyPropertiesName, queueFamilyProperties.timestampValidBits);

	char *extent3DName = (char*) malloc((strlen(queueFamilyPropertiesName) + 29) * sizeof(char));
	sprintf(extent3DName, "%s.%s", queueFamilyPropertiesName, "minImageTransferGranularity");
	vkuDescribeExtent3D(extent3DName, queueFamilyProperties.minImageTransferGranularity);
	free(extent3DName);
}

void vkuDescribeQueuesFamilyProperties(const char *physicalDeviceName, const uint32_t queueFamilyPropertiesCount, const VkQueueFamilyProperties *queueFamilyProperties)
{
	for(unsigned int i = 0; i < queueFamilyPropertiesCount; i++)
	{
		const char queueFamilyPropertiesLabel[] = "queue_family_properties_";
		char *queueFamilyPropertiesName = (char*) malloc((strlen(physicalDeviceName) + strlen(physicalDeviceName) + 4 + 32) * sizeof(char));
		sprintf(queueFamilyPropertiesName, "%s.%s(%i)", physicalDeviceName, queueFamilyPropertiesLabel, i);
		//fprintf(stdout, "%s:\n", queueFamilyPropertiesName);

		vkuDescribeQueueFamilyProperties(queueFamilyPropertiesName, queueFamilyProperties[i]);

		free(queueFamilyPropertiesName);
	}
}

void vkuDescribePhysicalDevice(const char *physicalDeviceName, const VkPhysicalDevice physicalDevice)
{
	VkPhysicalDeviceProperties physicalDeviceProperties;
	vkGetPhysicalDeviceProperties(physicalDevice, &physicalDeviceProperties);
	char *physicalDevicePropertiesName = (char*) malloc((strlen(physicalDeviceName) + 12) * sizeof(char));
	sprintf(physicalDevicePropertiesName, "%s.%s", physicalDeviceName, "properties");
	vkuDescribePhysicalDeviceProperties(physicalDevicePropertiesName, physicalDeviceProperties);
	free(physicalDevicePropertiesName);

	VkPhysicalDeviceFeatures physicalDeviceFeatures;
	vkGetPhysicalDeviceFeatures(physicalDevice, &physicalDeviceFeatures);
	char *physicalDeviceFeaturesName = (char*) malloc((strlen(physicalDeviceName) + 20) * sizeof(char));
	sprintf(physicalDeviceFeaturesName, "%s.%s", physicalDeviceName, "available_features");
	vkuDescribePhysicalDeviceFeatures(physicalDeviceFeaturesName, physicalDeviceFeatures);
	free(physicalDeviceFeaturesName);

	VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &physicalDeviceMemoryProperties);
	char *physicalDeviceMemoryPropertiesName = (char*) malloc((strlen(physicalDeviceName) + 19) * sizeof(char));
	sprintf(physicalDeviceMemoryPropertiesName, "%s.%s", physicalDeviceName, "memory_properties");
	vkuDescribePhysicalDeviceMemoryProperties(physicalDeviceMemoryPropertiesName, physicalDeviceMemoryProperties);
	free(physicalDeviceMemoryPropertiesName);

	uint32_t queueFamilyPropertiesCount = 0;
	VkQueueFamilyProperties *queueFamilyProperties = NULL;
	vkuGetQueueFamilyProperties(&physicalDevice, &queueFamilyPropertiesCount, &queueFamilyProperties);
	vkuDescribeQueuesFamilyProperties(physicalDeviceName, queueFamilyPropertiesCount, queueFamilyProperties);
	free(queueFamilyProperties);

	uint32_t layerPropertiesCount = 0;
	VkLayerProperties *layerProperties = NULL;
	vkuGetDeviceLayerProperties(&physicalDevice, &layerPropertiesCount, &layerProperties);
	vkuDescribeLayerProperties(physicalDeviceName, layerPropertiesCount, layerProperties);
	free(layerProperties);

	uint32_t extensionPropertiesCount = 0;
	VkExtensionProperties *extensionProperties = NULL;
	vkuGetDeviceExtensionProperties(&physicalDevice, &extensionPropertiesCount, &extensionProperties);
	vkuDescribeExtensionProperties(physicalDeviceName, extensionPropertiesCount, extensionProperties);
	free(extensionProperties);
}

void vkuDescribePhysicalDevices(const uint32_t physicalDevicesCount, const VkPhysicalDevice *physicalDevices)
{
	for(unsigned int i = 0; i < physicalDevicesCount; i++)
	{
		const char physicalDeviceLabel[] = "physical_device_";
		char *physicalDeviceName = (char*) malloc((strlen(physicalDeviceLabel) + 3 + 32) * sizeof(char));
		sprintf(physicalDeviceName, "%s(%i)", physicalDeviceLabel, i);

		fprintf(stdout, "%s:\n", physicalDeviceName);
		vkuDescribePhysicalDevice(physicalDeviceName, physicalDevices[i]);

		free(physicalDeviceName);
	}
}

void vkuListPhysicalDevicesNames(const uint32_t physicalDevicesCount, const VkPhysicalDevice *physicalDevices)
{
	for(unsigned int i = 0; i < physicalDevicesCount; i++)
	{
		const char physicalDeviceLabel[] = "physical_device_";
		char *physicalDeviceName = (char*) malloc((strlen(physicalDeviceLabel) + 3 + 32) * sizeof(char));
		sprintf(physicalDeviceName, "%s(%i)", physicalDeviceLabel, i);

		VkPhysicalDeviceProperties physicalDeviceProperties;
		vkGetPhysicalDeviceProperties(physicalDevices[i], &physicalDeviceProperties);
		fprintf(stdout, "%s : %s\n", physicalDeviceName, physicalDeviceProperties.deviceName);

		free(physicalDeviceName);
	}
}

void vkuDescribeLayerProperties(const char *physicalDeviceName, const uint32_t layerPropertiesCount, const VkLayerProperties *layerProperties)
{
	for(unsigned int i = 0; i < layerPropertiesCount; i++)
	{
		const char *localPhysicalDeviceName = "";
		int localPhysicalDeviceNameSize = 0;
		if(physicalDeviceName != NULL)
		{
			localPhysicalDeviceName = physicalDeviceName;
			localPhysicalDeviceNameSize = strlen(physicalDeviceName);
		}

		const char layerPropertiesLabel[] = "layer_properties_";

		char *layerPropertiesName;
		if(localPhysicalDeviceNameSize > 0)
		{
			layerPropertiesName = (char*) malloc((localPhysicalDeviceNameSize + 1 + strlen(layerPropertiesLabel) + 3 + 32) * sizeof(char));
			sprintf(layerPropertiesName, "%s.%s(%i)", localPhysicalDeviceName, layerPropertiesLabel, i);
		}
		else
		{
			layerPropertiesName = (char*) malloc((strlen(layerPropertiesLabel) + 3 + 32) * sizeof(char));
			sprintf(layerPropertiesName, "%s(%i)", layerPropertiesLabel, i);
		}

		fprintf(stdout, "%s.layerName = %s\n", layerPropertiesName, layerProperties[i].layerName);
		fprintf(stdout, "%s.specVersion = %u\n", layerPropertiesName, layerProperties[i].specVersion);
		fprintf(stdout, "%s.implementationVersion = %u\n", layerPropertiesName, layerProperties[i].implementationVersion);
		fprintf(stdout, "%s.description = %s\n", layerPropertiesName, layerProperties[i].description);

		free(layerPropertiesName);
	}
}

void vkuListLayerPropertiesNames(const uint32_t layerPropertiesCount, const VkLayerProperties *layerProperties)
{
	for(unsigned int i = 0; i < layerPropertiesCount; i++)
	{
		const char layerPropertiesLabel[] = "instance.layer_properties_";

		char *layerPropertiesName = (char*) malloc((strlen(layerPropertiesLabel) + 3 + 32) * sizeof(char));
		sprintf(layerPropertiesName, "%s(%i)", layerPropertiesLabel, i);

		fprintf(stdout, "%s : %s\n", layerPropertiesName, layerProperties[i].layerName);

		free(layerPropertiesName);
	}
}

void vkuDescribeExtensionProperties(const char *physicalDeviceName, const uint32_t extensionPropertiesCount, const VkExtensionProperties *extensionProperties)
{
	for(unsigned int i = 0; i < extensionPropertiesCount; i++)
	{
		const char *localExtensionDeviceName = "";
		int localPhysicalDeviceNameSize = 0;
		if(physicalDeviceName != NULL)
		{
			localExtensionDeviceName = physicalDeviceName;
			localPhysicalDeviceNameSize = strlen(physicalDeviceName);
		}

		const char extensionPropertiesLabel[] = "extension_properties_";

		char *extensionPropertiesName;
		if(localPhysicalDeviceNameSize > 0)
		{
			extensionPropertiesName = (char*) malloc((localPhysicalDeviceNameSize + 1 + strlen(extensionPropertiesLabel) + 3 + 32) * sizeof(char));
			sprintf(extensionPropertiesName, "%s.%s(%i)", localExtensionDeviceName, extensionPropertiesLabel, i);
		}
		else
		{
			extensionPropertiesName = (char*) malloc((strlen(extensionPropertiesLabel) + 3 + 32) * sizeof(char));
			sprintf(extensionPropertiesName, "%s(%i)", extensionPropertiesLabel, i);
		}

		fprintf(stdout, "%s.extensionName = %s\n", extensionPropertiesName, extensionProperties[i].extensionName);
		fprintf(stdout, "%s.specVersion = %u\n", extensionPropertiesName, extensionProperties[i].specVersion);

		free(extensionPropertiesName);
	}
}

