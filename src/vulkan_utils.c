/***********************************************************************/
/* Name        : vulkan_utils.c                                        */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#include <vulkan/vulkan.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "vulkan_utils.h"

VkResult CreateDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback)
{
	PFN_vkCreateDebugReportCallbackEXT func = (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
	if(func != NULL)
	{
		return func(instance, pCreateInfo, pAllocator, pCallback);
	}
	else
	{
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData)
{
	fprintf(stdout,  "validation layer: %s\n",  msg);
	return VK_FALSE;
}

void vkuVersion()
{
	unsigned int versionMajor = 1;
	unsigned int versionMinor = 1;
	unsigned int versionPatchMajor = 97;
	unsigned int versionPatchMinor = 0;
	fprintf(stdout, "vulkan_utils functions compatible with Vulkan version >= %u.%u.%u.%u\n", versionMajor, versionMinor, versionPatchMajor, versionPatchMinor);
}

