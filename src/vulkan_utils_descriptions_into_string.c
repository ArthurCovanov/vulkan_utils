/***********************************************************************/
/* Name        : vulkan_utils_descriptions.c                           */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#include <vulkan/vulkan.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "c_utils.h"
#include "vulkan_utils.h"

int vkuDescribeSampleCountFlagsIntoString(const VkSampleCountFlags sampleCountFlags, char **target)
{
	*target = NULL;
	bool support = false;
	int sampleCountBit[7] = { 0 };
	int i = 0;

	if((sampleCountFlags & VK_SAMPLE_COUNT_1_BIT) == VK_SAMPLE_COUNT_1_BIT)
		sampleCountBit[i++] = 1;
	if((sampleCountFlags & VK_SAMPLE_COUNT_2_BIT) == VK_SAMPLE_COUNT_2_BIT)
		sampleCountBit[i++] = 2;
	if((sampleCountFlags & VK_SAMPLE_COUNT_4_BIT) == VK_SAMPLE_COUNT_4_BIT)
		sampleCountBit[i++] = 4;
	if((sampleCountFlags & VK_SAMPLE_COUNT_8_BIT) == VK_SAMPLE_COUNT_8_BIT)
		sampleCountBit[i++] = 8;
	if((sampleCountFlags & VK_SAMPLE_COUNT_16_BIT) == VK_SAMPLE_COUNT_16_BIT)
		sampleCountBit[i++] = 16;
	if((sampleCountFlags & VK_SAMPLE_COUNT_32_BIT) == VK_SAMPLE_COUNT_32_BIT)
		sampleCountBit[i++] = 32;
	if((sampleCountFlags & VK_SAMPLE_COUNT_64_BIT) == VK_SAMPLE_COUNT_64_BIT)
		sampleCountBit[i++] = 64;

	char mask[(7 * (32 + 2) + 1)] = { 0 };
	for(i = 0; i < 7; i++)
	{
		if(sampleCountBit[i] > 0)
		{
			if(!support)
			{
				sprintf(&(mask[strlen(mask)]), "%i", sampleCountBit[i]);
				support = true;
			}
			else
				sprintf(&(mask[strlen(mask)]), ", %i", sampleCountBit[i]);
		}
	}
	mask[strlen(mask)] = '\0';
	return stprintf(target, "%s%s%s\n", "Can handle an image with {", mask, "} samples per pixel.");
}

int vkuDescribeMemoryPropertyFlagsIntoString(const VkMemoryPropertyFlags memoryPropertyFlags, char **target)
{
	*target = NULL;
	char mask[192] = { 0 };

	if((memoryPropertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
		sprintf(&(mask[strlen(mask)]), "%s", "VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT");
	if((memoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT");
	if((memoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) == VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_MEMORY_PROPERTY_HOST_COHERENT_BIT");
	if((memoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) == VK_MEMORY_PROPERTY_HOST_CACHED_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_MEMORY_PROPERTY_HOST_CACHED_BIT");
	if((memoryPropertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT) == VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT");
	mask[strlen(mask)] = '\0';

	if(strlen(mask)<=0)
		return stprintf(target, "NONE\n");
	else
		return stprintf(target, "%s\n", mask);
}

int vkuDescribeQueueFlagsIntoString(const VkQueueFlags queueFlags, char **target)
{
	char mask[99] = { 0 };

	if((queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT)
		sprintf(&(mask[strlen(mask)]), "%s", "VK_QUEUE_GRAPHICS_BIT");
	if((queueFlags & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_QUEUE_COMPUTE_BIT");
	if((queueFlags & VK_QUEUE_TRANSFER_BIT) == VK_QUEUE_TRANSFER_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_QUEUE_TRANSFER_BIT");
	if((queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) == VK_QUEUE_SPARSE_BINDING_BIT)
		sprintf(&(mask[strlen(mask)]), "%s%s", ((strlen(mask) > 0)? " | " : ""), "VK_QUEUE_SPARSE_BINDING_BIT");
	mask[strlen(mask)] = '\0';

	if(strlen(mask)<=0)
		return stprintf(target, "NONE\n");
	else
		return stprintf(target, "%s\n", mask);
}

int vkuDescribeMemoryHeapFlagsIntoString(const VkMemoryHeapFlags memoryHeapFlags, char **target)
{
	char mask[32] = { 0 };

	if((memoryHeapFlags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) == VK_MEMORY_HEAP_DEVICE_LOCAL_BIT)
		sprintf(&(mask[strlen(mask)]), "%s", "VK_MEMORY_HEAP_DEVICE_LOCAL_BIT");
	mask[strlen(mask)] = '\0';

	if(strlen(mask)<=0)
		return stprintf(target, "NONE\n");
	else
		return stprintf(target, "%s\n", mask);
}

int vkuDescribePhysicalDeviceLimitsPropertiesIntoString(const char *physicalDeviceLimitsName, const VkPhysicalDeviceLimits physicalDeviceLimits, char **target)
{
	int length = 0;
	*target = NULL;
	char* maxImageDimension1D = NULL;
	char* maxImageDimension2D = NULL;
	char* maxImageDimension3D = NULL;
	char* maxImageDimensionCube = NULL;
	char* maxImageArrayLayers = NULL;
	char* maxTexelBufferElements = NULL;
	char* maxUniformBufferRange = NULL;
	char* maxStorageBufferRange = NULL;
	char* maxPushConstantsSize = NULL;
	char* maxMemoryAllocationCount = NULL;
	char* maxSamplerAllocationCount = NULL;
	char* bufferImageGranularity = NULL;
	char* sparseAddressSpaceSize = NULL;
	char* maxBoundDescriptorSets = NULL;
	char* maxPerStageDescriptorSamplers = NULL;
	char* maxPerStageDescriptorUniformBuffers = NULL;
	char* maxPerStageDescriptorStorageBuffers = NULL;
	char* maxPerStageDescriptorSampledImages = NULL;
	char* maxPerStageDescriptorStorageImages = NULL;
	char* maxPerStageDescriptorInputAttachments = NULL;
	char* maxPerStageResources = NULL;
	char* maxDescriptorSetSamplers = NULL;
	char* maxDescriptorSetUniformBuffers = NULL;
	char* maxDescriptorSetUniformBuffersDynamic = NULL;
	char* maxDescriptorSetStorageBuffers = NULL;
	char* maxDescriptorSetStorageBuffersDynamic = NULL;
	char* maxDescriptorSetSampledImages = NULL;
	char* maxDescriptorSetStorageImages = NULL;
	char* maxDescriptorSetInputAttachments = NULL;
	char* maxVertexInputAttributes = NULL;
	char* maxVertexInputBindings = NULL;
	char* maxVertexInputAttributeOffset = NULL;
	char* maxVertexInputBindingStride = NULL;
	char* maxVertexOutputComponents = NULL;
	char* maxTessellationGenerationLevel = NULL;
	char* maxTessellationPatchSize = NULL;
	char* maxTessellationControlPerVertexInputComponents = NULL;
	char* maxTessellationControlPerVertexOutputComponents = NULL;
	char* maxTessellationControlPerPatchOutputComponents = NULL;
	char* maxTessellationControlTotalOutputComponents = NULL;
	char* maxTessellationEvaluationInputComponents = NULL;
	char* maxTessellationEvaluationOutputComponents = NULL;
	char* maxGeometryShaderInvocations = NULL;
	char* maxGeometryInputComponents = NULL;
	char* maxGeometryOutputComponents = NULL;
	char* maxGeometryOutputVertices = NULL;
	char* maxGeometryTotalOutputComponents = NULL;
	char* maxFragmentInputComponents = NULL;
	char* maxFragmentOutputAttachments = NULL;
	char* maxFragmentDualSrcAttachments = NULL;
	char* maxFragmentCombinedOutputResources = NULL;
	char* maxComputeSharedMemorySize = NULL;
	char* maxComputeWorkGroupCount0 = NULL;
	char* maxComputeWorkGroupCount1 = NULL;
	char* maxComputeWorkGroupCount2 = NULL;
	char* maxComputeWorkGroupInvocations = NULL;
	char* maxComputeWorkGroupSize0 = NULL;
	char* maxComputeWorkGroupSize1 = NULL;
	char* maxComputeWorkGroupSize2 = NULL;
	char* subPixelPrecisionBits = NULL;
	char* subTexelPrecisionBits = NULL;
	char* mipmapPrecisionBits = NULL;
	char* maxDrawIndexedIndexValue = NULL;
	char* maxDrawIndirectCount = NULL;
	char* maxSamplerLodBias = NULL;
	char* maxSamplerAnisotropy = NULL;
	char* maxViewports = NULL;
	char* maxViewportDimensions0 = NULL;
	char* maxViewportDimensions1 = NULL;
	char* viewportBoundsRange0 = NULL;
	char* viewportBoundsRange1 = NULL;
	char* viewportSubPixelBits = NULL;
	char* minMemoryMapAlignment = NULL;
	char* minTexelBufferOffsetAlignment = NULL;
	char* minUniformBufferOffsetAlignment = NULL;
	char* minStorageBufferOffsetAlignment = NULL;
	char* minTexelOffset = NULL;
	char* maxTexelOffset = NULL;
	char* minTexelGatherOffset = NULL;
	char* maxTexelGatherOffset = NULL;
	char* minInterpolationOffset = NULL;
	char* maxInterpolationOffset = NULL;
	char* subPixelInterpolationOffsetBits = NULL;
	char* maxFramebufferWidth = NULL;
	char* maxFramebufferHeight = NULL;
	char* maxFramebufferLayers = NULL;
	char* framebufferColorSampleCounts = NULL;
	char* framebufferColorSampleCountsDescription = NULL;
	char* framebufferDepthSampleCounts = NULL;
	char* framebufferDepthSampleCountsDescription = NULL;
	char* framebufferStencilSampleCounts = NULL;
	char* framebufferStencilSampleCountsDescription = NULL;
	char* framebufferNoAttachmentsSampleCounts = NULL;
	char* framebufferNoAttachmentsSampleCountsDescription = NULL;
	char* maxColorAttachments = NULL;
	char* sampledImageColorSampleCounts = NULL;
	char* sampledImageColorSampleCountsDescription = NULL;
	char* sampledImageIntegerSampleCounts = NULL;
	char* sampledImageIntegerSampleCountsDescription = NULL;
	char* sampledImageDepthSampleCounts = NULL;
	char* sampledImageDepthSampleCountsDescription = NULL;
	char* sampledImageStencilSampleCounts = NULL;
	char* sampledImageStencilSampleCountsDescription = NULL;
	char* storageImageSampleCounts = NULL;
	char* storageImageSampleCountsDescription = NULL;
	char* maxSampleMaskWords = NULL;
	char* timestampComputeAndGraphics = NULL;
	char* timestampPeriod  = NULL;
	char* maxClipDistances = NULL;
	char* maxCullDistances = NULL;
	char* maxCombinedClipAndCullDistances = NULL;
	char* discreteQueuePriorities = NULL;
	char* pointSizeRange0 = NULL;
	char* pointSizeRange1 = NULL;
	char* lineWidthRange0 = NULL;
	char* lineWidthRange1 = NULL;
	char* pointSizeGranularity  = NULL;
	char* lineWidthGranularity = NULL;
	char* strictLines = NULL;
	char* standardSampleLocations = NULL;
	char* optimalBufferCopyOffsetAlignment = NULL;
	char* optimalBufferCopyRowPitchAlignment = NULL;
	char* nonCoherentAtomSize = NULL;

	stprintf(&maxImageDimension1D, "%s.maxImageDimension1D = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxImageDimension1D);
	stprintf(&maxImageDimension2D, "%s.maxImageDimension2D = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxImageDimension2D);
	stprintf(&maxImageDimension3D, "%s.maxImageDimension3D = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxImageDimension3D);
	stprintf(&maxImageDimensionCube, "%s.maxImageDimensionCube = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxImageDimensionCube);
	stprintf(&maxImageArrayLayers, "%s.maxImageArrayLayers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxImageArrayLayers);
	stprintf(&maxTexelBufferElements, "%s.maxTexelBufferElements = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTexelBufferElements);
	stprintf(&maxUniformBufferRange, "%s.maxUniformBufferRange = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxUniformBufferRange);
	stprintf(&maxStorageBufferRange, "%s.maxStorageBufferRange = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxStorageBufferRange);
	stprintf(&maxPushConstantsSize, "%s.maxPushConstantsSize = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPushConstantsSize);
	stprintf(&maxMemoryAllocationCount, "%s.maxMemoryAllocationCount = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxMemoryAllocationCount);
	stprintf(&maxSamplerAllocationCount, "%s.maxSamplerAllocationCount = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxSamplerAllocationCount);
	stprintf(&bufferImageGranularity, "%s.bufferImageGranularity = %lu\n", physicalDeviceLimitsName, physicalDeviceLimits.bufferImageGranularity);
	stprintf(&sparseAddressSpaceSize, "%s.sparseAddressSpaceSize = %lu\n", physicalDeviceLimitsName, physicalDeviceLimits.sparseAddressSpaceSize);
	stprintf(&maxBoundDescriptorSets, "%s.maxBoundDescriptorSets = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxBoundDescriptorSets);
	stprintf(&maxPerStageDescriptorSamplers, "%s.maxPerStageDescriptorSamplers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorSamplers);
	stprintf(&maxPerStageDescriptorUniformBuffers, "%s.maxPerStageDescriptorUniformBuffers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorUniformBuffers);
	stprintf(&maxPerStageDescriptorStorageBuffers, "%s.maxPerStageDescriptorStorageBuffers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorStorageBuffers);
	stprintf(&maxPerStageDescriptorSampledImages, "%s.maxPerStageDescriptorSampledImages = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorSampledImages);
	stprintf(&maxPerStageDescriptorStorageImages, "%s.maxPerStageDescriptorStorageImages = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorStorageImages);
	stprintf(&maxPerStageDescriptorInputAttachments, "%s.maxPerStageDescriptorInputAttachments = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageDescriptorInputAttachments);
	stprintf(&maxPerStageResources, "%s.maxPerStageResources = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxPerStageResources);
	stprintf(&maxDescriptorSetSamplers, "%s.maxDescriptorSetSamplers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetSamplers);
	stprintf(&maxDescriptorSetUniformBuffers, "%s.maxDescriptorSetUniformBuffers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetUniformBuffers);
	stprintf(&maxDescriptorSetUniformBuffersDynamic, "%s.maxDescriptorSetUniformBuffersDynamic = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetUniformBuffersDynamic);
	stprintf(&maxDescriptorSetStorageBuffers, "%s.maxDescriptorSetStorageBuffers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetStorageBuffers);
	stprintf(&maxDescriptorSetStorageBuffersDynamic, "%s.maxDescriptorSetStorageBuffersDynamic = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetStorageBuffersDynamic);
	stprintf(&maxDescriptorSetSampledImages, "%s.maxDescriptorSetSampledImages = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetSampledImages);
	stprintf(&maxDescriptorSetStorageImages, "%s.maxDescriptorSetStorageImages = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetStorageImages);
	stprintf(&maxDescriptorSetInputAttachments, "%s.maxDescriptorSetInputAttachments = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDescriptorSetInputAttachments);
	stprintf(&maxVertexInputAttributes, "%s.maxVertexInputAttributes = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxVertexInputAttributes);
	stprintf(&maxVertexInputBindings, "%s.maxVertexInputBindings = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxVertexInputBindings);
	stprintf(&maxVertexInputAttributeOffset, "%s.maxVertexInputAttributeOffset = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxVertexInputAttributeOffset);
	stprintf(&maxVertexInputBindingStride, "%s.maxVertexInputBindingStride = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxVertexInputBindingStride);
	stprintf(&maxVertexOutputComponents, "%s.maxVertexOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxVertexOutputComponents);
	stprintf(&maxTessellationGenerationLevel, "%s.maxTessellationGenerationLevel = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationGenerationLevel);
	stprintf(&maxTessellationPatchSize, "%s.maxTessellationPatchSize = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationPatchSize);
	stprintf(&maxTessellationControlPerVertexInputComponents, "%s.maxTessellationControlPerVertexInputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationControlPerVertexInputComponents);
	stprintf(&maxTessellationControlPerVertexOutputComponents, "%s.maxTessellationControlPerVertexOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationControlPerVertexOutputComponents);
	stprintf(&maxTessellationControlPerPatchOutputComponents, "%s.maxTessellationControlPerPatchOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationControlPerPatchOutputComponents);
	stprintf(&maxTessellationControlTotalOutputComponents, "%s.maxTessellationControlTotalOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationControlTotalOutputComponents);
	stprintf(&maxTessellationEvaluationInputComponents, "%s.maxTessellationEvaluationInputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationEvaluationInputComponents);
	stprintf(&maxTessellationEvaluationOutputComponents, "%s.maxTessellationEvaluationOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTessellationEvaluationOutputComponents);
	stprintf(&maxGeometryShaderInvocations, "%s.maxGeometryShaderInvocations = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxGeometryShaderInvocations);
	stprintf(&maxGeometryInputComponents, "%s.maxGeometryInputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxGeometryInputComponents);
	stprintf(&maxGeometryOutputComponents, "%s.maxGeometryOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxGeometryOutputComponents);
	stprintf(&maxGeometryOutputVertices, "%s.maxGeometryOutputVertices = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxGeometryOutputVertices);
	stprintf(&maxGeometryTotalOutputComponents, "%s.maxGeometryTotalOutputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxGeometryTotalOutputComponents);
	stprintf(&maxFragmentInputComponents, "%s.maxFragmentInputComponents = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFragmentInputComponents);
	stprintf(&maxFragmentOutputAttachments, "%s.maxFragmentOutputAttachments = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFragmentOutputAttachments);
	stprintf(&maxFragmentDualSrcAttachments, "%s.maxFragmentDualSrcAttachments = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFragmentDualSrcAttachments);
	stprintf(&maxFragmentCombinedOutputResources, "%s.maxFragmentCombinedOutputResources = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFragmentCombinedOutputResources);
	stprintf(&maxComputeSharedMemorySize, "%s.maxComputeSharedMemorySize = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeSharedMemorySize);
	stprintf(&maxComputeWorkGroupCount0, "%s.maxComputeWorkGroupCount[0] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupCount[0]);
	stprintf(&maxComputeWorkGroupCount1, "%s.maxComputeWorkGroupCount[1] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupCount[1]);
	stprintf(&maxComputeWorkGroupCount2, "%s.maxComputeWorkGroupCount[2] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupCount[2]);
	stprintf(&maxComputeWorkGroupInvocations, "%s.maxComputeWorkGroupInvocations = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupInvocations);
	stprintf(&maxComputeWorkGroupSize0, "%s.maxComputeWorkGroupSize[0] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupSize[0]);
	stprintf(&maxComputeWorkGroupSize1, "%s.maxComputeWorkGroupSize[1] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupSize[1]);
	stprintf(&maxComputeWorkGroupSize2, "%s.maxComputeWorkGroupSize[2] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxComputeWorkGroupSize[2]);
	stprintf(&subPixelPrecisionBits, "%s.subPixelPrecisionBits = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.subPixelPrecisionBits);
	stprintf(&subTexelPrecisionBits, "%s.subTexelPrecisionBits = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.subTexelPrecisionBits);
	stprintf(&mipmapPrecisionBits, "%s.mipmapPrecisionBits = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.mipmapPrecisionBits);
	stprintf(&maxDrawIndexedIndexValue, "%s.maxDrawIndexedIndexValue = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDrawIndexedIndexValue);
	stprintf(&maxDrawIndirectCount, "%s.maxDrawIndirectCount = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxDrawIndirectCount);
	stprintf(&maxSamplerLodBias, "%s.maxSamplerLodBias = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.maxSamplerLodBias);
	stprintf(&maxSamplerAnisotropy, "%s.maxSamplerAnisotropy = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.maxSamplerAnisotropy);
	stprintf(&maxViewports, "%s.maxViewports = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxViewports);
	stprintf(&maxViewportDimensions0, "%s.maxViewportDimensions[0] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxViewportDimensions[0]);
	stprintf(&maxViewportDimensions1, "%s.maxViewportDimensions[1] = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxViewportDimensions[1]);
	stprintf(&viewportBoundsRange0, "%s.viewportBoundsRange[0] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.viewportBoundsRange[0]);
	stprintf(&viewportBoundsRange1, "%s.viewportBoundsRange[1] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.viewportBoundsRange[1]);
	stprintf(&viewportSubPixelBits, "%s.viewportSubPixelBits = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.viewportSubPixelBits);
	stprintf(&minMemoryMapAlignment, "%s.minMemoryMapAlignment = %lu\n", physicalDeviceLimitsName, physicalDeviceLimits.minMemoryMapAlignment);
	stprintf(&minTexelBufferOffsetAlignment, "%s.minTexelBufferOffsetAlignment = %lu\n", physicalDeviceLimitsName, physicalDeviceLimits.minTexelBufferOffsetAlignment);
	stprintf(&minUniformBufferOffsetAlignment, "%s.minUniformBufferOffsetAlignment = %lu\n", physicalDeviceLimitsName, physicalDeviceLimits.minUniformBufferOffsetAlignment);
	stprintf(&minStorageBufferOffsetAlignment, "%s.minStorageBufferOffsetAlignment = %lu\n", physicalDeviceLimitsName, physicalDeviceLimits.minStorageBufferOffsetAlignment);
	stprintf(&minTexelOffset, "%s.minTexelOffset = %i\n", physicalDeviceLimitsName, physicalDeviceLimits.minTexelOffset);
	stprintf(&maxTexelOffset, "%s.maxTexelOffset = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTexelOffset);
	stprintf(&minTexelGatherOffset, "%s.minTexelGatherOffset = %i\n", physicalDeviceLimitsName, physicalDeviceLimits.minTexelGatherOffset);
	stprintf(&maxTexelGatherOffset, "%s.maxTexelGatherOffset = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxTexelGatherOffset);
	stprintf(&minInterpolationOffset, "%s.minInterpolationOffset = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.viewportBoundsRange[1]);
	stprintf(&maxInterpolationOffset, "%s.maxInterpolationOffset = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.viewportBoundsRange[1]);
	stprintf(&subPixelInterpolationOffsetBits, "%s.subPixelInterpolationOffsetBits = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.subPixelInterpolationOffsetBits);
	stprintf(&maxFramebufferWidth, "%s.maxFramebufferWidth = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFramebufferWidth);
	stprintf(&maxFramebufferHeight, "%s.maxFramebufferHeight = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFramebufferHeight);
	stprintf(&maxFramebufferLayers, "%s.maxFramebufferLayers = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxFramebufferLayers);
	stprintf(&framebufferColorSampleCounts, "%s.framebufferColorSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.framebufferColorSampleCounts);
	vkuDescribeSampleCountFlagsIntoString(physicalDeviceLimits.framebufferColorSampleCounts, &framebufferColorSampleCountsDescription);
	stprintf(&framebufferDepthSampleCounts, "%s.framebufferDepthSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.framebufferDepthSampleCounts);
	vkuDescribeSampleCountFlagsIntoString(physicalDeviceLimits.framebufferDepthSampleCounts, &framebufferDepthSampleCountsDescription);
	stprintf(&framebufferStencilSampleCounts, "%s.framebufferStencilSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.framebufferStencilSampleCounts);
	vkuDescribeSampleCountFlagsIntoString(physicalDeviceLimits.framebufferStencilSampleCounts, &framebufferStencilSampleCountsDescription);
	stprintf(&framebufferNoAttachmentsSampleCounts, "%s.framebufferNoAttachmentsSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.framebufferNoAttachmentsSampleCounts);
	vkuDescribeSampleCountFlagsIntoString(physicalDeviceLimits.framebufferNoAttachmentsSampleCounts, &framebufferNoAttachmentsSampleCountsDescription);
	stprintf(&maxColorAttachments, "%s.maxColorAttachments = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxColorAttachments);
	stprintf(&sampledImageColorSampleCounts, "%s.sampledImageColorSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.sampledImageColorSampleCounts);
	vkuDescribeSampleCountFlagsIntoString(physicalDeviceLimits.sampledImageColorSampleCounts, &sampledImageColorSampleCountsDescription);
	stprintf(&sampledImageIntegerSampleCounts, "%s.sampledImageIntegerSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.sampledImageIntegerSampleCounts);
	vkuDescribeSampleCountFlagsIntoString(physicalDeviceLimits.sampledImageIntegerSampleCounts, &sampledImageIntegerSampleCountsDescription);
	stprintf(&sampledImageDepthSampleCounts, "%s.sampledImageDepthSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.sampledImageDepthSampleCounts);
	vkuDescribeSampleCountFlagsIntoString(physicalDeviceLimits.sampledImageDepthSampleCounts, &sampledImageDepthSampleCountsDescription);
	stprintf(&sampledImageStencilSampleCounts, "%s.sampledImageStencilSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.sampledImageStencilSampleCounts);
	vkuDescribeSampleCountFlagsIntoString(physicalDeviceLimits.sampledImageStencilSampleCounts, &sampledImageStencilSampleCountsDescription);
	stprintf(&storageImageSampleCounts, "%s.storageImageSampleCounts = 0x%08X : ", physicalDeviceLimitsName, physicalDeviceLimits.storageImageSampleCounts);
	vkuDescribeSampleCountFlagsIntoString(physicalDeviceLimits.storageImageSampleCounts, &storageImageSampleCountsDescription);
	stprintf(&maxSampleMaskWords, "%s.maxSampleMaskWords = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxSampleMaskWords);
	stprintf(&timestampComputeAndGraphics, "%s.timestampComputeAndGraphics = %s\n", physicalDeviceLimitsName, ((physicalDeviceLimits.timestampComputeAndGraphics == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&timestampPeriod, "%s.timestampPeriod = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.timestampPeriod);
	stprintf(&maxClipDistances, "%s.maxClipDistances = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxClipDistances);
	stprintf(&maxCullDistances, "%s.maxCullDistances = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxCullDistances);
	stprintf(&maxCombinedClipAndCullDistances, "%s.maxCombinedClipAndCullDistances = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.maxCombinedClipAndCullDistances);
	stprintf(&discreteQueuePriorities, "%s.discreteQueuePriorities = %u\n", physicalDeviceLimitsName, physicalDeviceLimits.discreteQueuePriorities);
	stprintf(&pointSizeRange0, "%s.pointSizeRange[0] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.pointSizeRange[0]);
	stprintf(&pointSizeRange1, "%s.pointSizeRange[1] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.pointSizeRange[1]);
	stprintf(&lineWidthRange0, "%s.lineWidthRange[0] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.lineWidthRange[0]);
	stprintf(&lineWidthRange1, "%s.lineWidthRange[1] = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.lineWidthRange[1]);
	stprintf(&pointSizeGranularity, "%s.pointSizeGranularity = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.pointSizeGranularity);
	stprintf(&lineWidthGranularity, "%s.lineWidthGranularity = %f\n", physicalDeviceLimitsName, physicalDeviceLimits.lineWidthGranularity);
	stprintf(&strictLines, "%s.strictLines = %s\n", physicalDeviceLimitsName, ((physicalDeviceLimits.strictLines == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&standardSampleLocations, "%s.standardSampleLocations = %s\n", physicalDeviceLimitsName, ((physicalDeviceLimits.standardSampleLocations == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&optimalBufferCopyOffsetAlignment, "%s.optimalBufferCopyOffsetAlignment = %lu\n", physicalDeviceLimitsName, physicalDeviceLimits.optimalBufferCopyOffsetAlignment);
	stprintf(&optimalBufferCopyRowPitchAlignment, "%s.optimalBufferCopyRowPitchAlignment = %lu\n", physicalDeviceLimitsName, physicalDeviceLimits.optimalBufferCopyRowPitchAlignment);
	stprintf(&nonCoherentAtomSize, "%s.nonCoherentAtomSize = %lu\n", physicalDeviceLimitsName, physicalDeviceLimits.nonCoherentAtomSize);

	length = stprintf(target, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", maxImageDimension1D, maxImageDimension2D, maxImageDimension3D, maxImageDimensionCube, maxImageArrayLayers, maxTexelBufferElements, maxUniformBufferRange, maxStorageBufferRange, maxPushConstantsSize, maxMemoryAllocationCount, maxSamplerAllocationCount, bufferImageGranularity, sparseAddressSpaceSize, maxBoundDescriptorSets, maxPerStageDescriptorSamplers, maxPerStageDescriptorUniformBuffers, maxPerStageDescriptorStorageBuffers, maxPerStageDescriptorSampledImages, maxPerStageDescriptorStorageImages, maxPerStageDescriptorInputAttachments, maxPerStageResources, maxDescriptorSetSamplers, maxDescriptorSetUniformBuffers, maxDescriptorSetUniformBuffersDynamic, maxDescriptorSetStorageBuffers, maxDescriptorSetStorageBuffersDynamic, maxDescriptorSetSampledImages, maxDescriptorSetStorageImages, maxDescriptorSetInputAttachments, maxVertexInputAttributes, maxVertexInputBindings, maxVertexInputAttributeOffset, maxVertexInputBindingStride, maxVertexOutputComponents, maxTessellationGenerationLevel, maxTessellationPatchSize, maxTessellationControlPerVertexInputComponents, maxTessellationControlPerVertexOutputComponents, maxTessellationControlPerPatchOutputComponents, maxTessellationControlTotalOutputComponents, maxTessellationEvaluationInputComponents, maxTessellationEvaluationOutputComponents, maxGeometryShaderInvocations, maxGeometryInputComponents, maxGeometryOutputComponents, maxGeometryOutputVertices, maxGeometryTotalOutputComponents, maxFragmentInputComponents, maxFragmentOutputAttachments, maxFragmentDualSrcAttachments, maxFragmentCombinedOutputResources, maxComputeSharedMemorySize, maxComputeWorkGroupCount0, maxComputeWorkGroupCount1, maxComputeWorkGroupCount2, maxComputeWorkGroupInvocations, maxComputeWorkGroupSize0, maxComputeWorkGroupSize1, maxComputeWorkGroupSize2, subPixelPrecisionBits, subTexelPrecisionBits, mipmapPrecisionBits, maxDrawIndexedIndexValue, maxDrawIndirectCount, maxSamplerLodBias, maxSamplerAnisotropy, maxViewports, maxViewportDimensions0, maxViewportDimensions1, viewportBoundsRange0, viewportBoundsRange1, viewportSubPixelBits, minMemoryMapAlignment, minTexelBufferOffsetAlignment, minUniformBufferOffsetAlignment, minStorageBufferOffsetAlignment, minTexelOffset, maxTexelOffset, minTexelGatherOffset, maxTexelGatherOffset, minInterpolationOffset, maxInterpolationOffset, subPixelInterpolationOffsetBits, maxFramebufferWidth, maxFramebufferHeight, maxFramebufferLayers, framebufferColorSampleCounts, framebufferColorSampleCountsDescription, framebufferDepthSampleCounts, framebufferDepthSampleCountsDescription, framebufferStencilSampleCounts, framebufferStencilSampleCountsDescription, framebufferNoAttachmentsSampleCounts, framebufferNoAttachmentsSampleCountsDescription, maxColorAttachments, sampledImageColorSampleCounts, sampledImageColorSampleCountsDescription, sampledImageIntegerSampleCounts, sampledImageIntegerSampleCountsDescription, sampledImageDepthSampleCounts, sampledImageDepthSampleCountsDescription, sampledImageStencilSampleCounts, sampledImageStencilSampleCountsDescription, storageImageSampleCounts, storageImageSampleCountsDescription, maxSampleMaskWords, timestampComputeAndGraphics, timestampPeriod , maxClipDistances, maxCullDistances, maxCombinedClipAndCullDistances, discreteQueuePriorities, pointSizeRange0, pointSizeRange1, lineWidthRange0, lineWidthRange1, pointSizeGranularity , lineWidthGranularity, strictLines, standardSampleLocations, optimalBufferCopyOffsetAlignment, optimalBufferCopyRowPitchAlignment, nonCoherentAtomSize);

	free(maxImageDimension1D);
	free(maxImageDimension2D);
	free(maxImageDimension3D);
	free(maxImageDimensionCube);
	free(maxImageArrayLayers);
	free(maxTexelBufferElements);
	free(maxUniformBufferRange);
	free(maxStorageBufferRange);
	free(maxPushConstantsSize);
	free(maxMemoryAllocationCount);
	free(maxSamplerAllocationCount);
	free(bufferImageGranularity);
	free(sparseAddressSpaceSize);
	free(maxBoundDescriptorSets);
	free(maxPerStageDescriptorSamplers);
	free(maxPerStageDescriptorUniformBuffers);
	free(maxPerStageDescriptorStorageBuffers);
	free(maxPerStageDescriptorSampledImages);
	free(maxPerStageDescriptorStorageImages);
	free(maxPerStageDescriptorInputAttachments);
	free(maxPerStageResources);
	free(maxDescriptorSetSamplers);
	free(maxDescriptorSetUniformBuffers);
	free(maxDescriptorSetUniformBuffersDynamic);
	free(maxDescriptorSetStorageBuffers);
	free(maxDescriptorSetStorageBuffersDynamic);
	free(maxDescriptorSetSampledImages);
	free(maxDescriptorSetStorageImages);
	free(maxDescriptorSetInputAttachments);
	free(maxVertexInputAttributes);
	free(maxVertexInputBindings);
	free(maxVertexInputAttributeOffset);
	free(maxVertexInputBindingStride);
	free(maxVertexOutputComponents);
	free(maxTessellationGenerationLevel);
	free(maxTessellationPatchSize);
	free(maxTessellationControlPerVertexInputComponents);
	free(maxTessellationControlPerVertexOutputComponents);
	free(maxTessellationControlPerPatchOutputComponents);
	free(maxTessellationControlTotalOutputComponents);
	free(maxTessellationEvaluationInputComponents);
	free(maxTessellationEvaluationOutputComponents);
	free(maxGeometryShaderInvocations);
	free(maxGeometryInputComponents);
	free(maxGeometryOutputComponents);
	free(maxGeometryOutputVertices);
	free(maxGeometryTotalOutputComponents);
	free(maxFragmentInputComponents);
	free(maxFragmentOutputAttachments);
	free(maxFragmentDualSrcAttachments);
	free(maxFragmentCombinedOutputResources);
	free(maxComputeSharedMemorySize);
	free(maxComputeWorkGroupCount0);
	free(maxComputeWorkGroupCount1);
	free(maxComputeWorkGroupCount2);
	free(maxComputeWorkGroupInvocations);
	free(maxComputeWorkGroupSize0);
	free(maxComputeWorkGroupSize1);
	free(maxComputeWorkGroupSize2);
	free(subPixelPrecisionBits);
	free(subTexelPrecisionBits);
	free(mipmapPrecisionBits);
	free(maxDrawIndexedIndexValue);
	free(maxDrawIndirectCount);
	free(maxSamplerLodBias);
	free(maxSamplerAnisotropy);
	free(maxViewports);
	free(maxViewportDimensions0);
	free(maxViewportDimensions1);
	free(viewportBoundsRange0);
	free(viewportBoundsRange1);
	free(viewportSubPixelBits);
	free(minMemoryMapAlignment);
	free(minTexelBufferOffsetAlignment);
	free(minUniformBufferOffsetAlignment);
	free(minStorageBufferOffsetAlignment);
	free(minTexelOffset);
	free(maxTexelOffset);
	free(minTexelGatherOffset);
	free(maxTexelGatherOffset);
	free(minInterpolationOffset);
	free(maxInterpolationOffset);
	free(subPixelInterpolationOffsetBits);
	free(maxFramebufferWidth);
	free(maxFramebufferHeight);
	free(maxFramebufferLayers);
	free(framebufferColorSampleCounts);
	free(framebufferColorSampleCountsDescription);
	free(framebufferDepthSampleCounts);
	free(framebufferDepthSampleCountsDescription);
	free(framebufferStencilSampleCounts);
	free(framebufferStencilSampleCountsDescription);
	free(framebufferNoAttachmentsSampleCounts);
	free(framebufferNoAttachmentsSampleCountsDescription);
	free(maxColorAttachments);
	free(sampledImageColorSampleCounts);
	free(sampledImageColorSampleCountsDescription);
	free(sampledImageIntegerSampleCounts);
	free(sampledImageIntegerSampleCountsDescription);
	free(sampledImageDepthSampleCounts);
	free(sampledImageDepthSampleCountsDescription);
	free(sampledImageStencilSampleCounts);
	free(sampledImageStencilSampleCountsDescription);
	free(storageImageSampleCounts);
	free(storageImageSampleCountsDescription);
	free(maxSampleMaskWords);
	free(timestampComputeAndGraphics);
	free(timestampPeriod );
	free(maxClipDistances);
	free(maxCullDistances);
	free(maxCombinedClipAndCullDistances);
	free(discreteQueuePriorities);
	free(pointSizeRange0);
	free(pointSizeRange1);
	free(lineWidthRange0);
	free(lineWidthRange1);
	free(pointSizeGranularity );
	free(lineWidthGranularity);
	free(strictLines);
	free(standardSampleLocations);
	free(optimalBufferCopyOffsetAlignment);
	free(optimalBufferCopyRowPitchAlignment);
	free(nonCoherentAtomSize);

	return length;
}

int vkuDescribePhysicalDeviceSparsePropertiesIntoString(const char *physicalDeviceSparsePropertiesName, const VkPhysicalDeviceSparseProperties physicalDeviceSparseProperties, char **target)
{
	int length = 0;
	*target = NULL;
	char* residencyStandard2DBlockShape = NULL;
	char* residencyStandard2DMultisampleBlockShape = NULL;
	char* residencyStandard3DBlockShape = NULL;
	char* residencyAlignedMipSize = NULL;
	char* residencyNonResidentStrict = NULL;
	stprintf(&residencyStandard2DBlockShape,            "%s.residencyStandard2DBlockShape            = %s\n", physicalDeviceSparsePropertiesName, ((physicalDeviceSparseProperties.residencyStandard2DBlockShape == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&residencyStandard2DMultisampleBlockShape, "%s.residencyStandard2DMultisampleBlockShape = %s\n", physicalDeviceSparsePropertiesName, ((physicalDeviceSparseProperties.residencyStandard2DMultisampleBlockShape == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&residencyStandard3DBlockShape,            "%s.residencyStandard3DBlockShape            = %s\n", physicalDeviceSparsePropertiesName, ((physicalDeviceSparseProperties.residencyStandard3DBlockShape == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&residencyAlignedMipSize,                  "%s.residencyAlignedMipSize                  = %s\n", physicalDeviceSparsePropertiesName, ((physicalDeviceSparseProperties.residencyAlignedMipSize == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&residencyNonResidentStrict,               "%s.residencyNonResidentStrict               = %s\n", physicalDeviceSparsePropertiesName, ((physicalDeviceSparseProperties.residencyNonResidentStrict == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));

	length = stprintf(target, "%s%s%s%s%s", residencyStandard2DBlockShape, residencyStandard2DMultisampleBlockShape, residencyStandard3DBlockShape, residencyAlignedMipSize, residencyNonResidentStrict);

	free(residencyStandard2DBlockShape);
	free(residencyStandard2DMultisampleBlockShape);
	free(residencyStandard3DBlockShape);
	free(residencyAlignedMipSize);
	free(residencyNonResidentStrict);

	return length;
}

int vkuDescribePhysicalDevicePropertiesIntoString(const char *physicalDevicePropertiesName, const VkPhysicalDeviceProperties physicalDeviceProperties, char **target)
{
	int length = 0;
	*target = NULL;
	char* apiVersion = NULL;
	char* driverVersion = NULL;
	char* vendorID = NULL;
	char* deviceID = NULL;
	char* deviceType = NULL;
	char* deviceName = NULL;
	stprintf(&apiVersion,    "%s.apiVersion = 0x%08X\n", physicalDevicePropertiesName, physicalDeviceProperties.apiVersion);
	stprintf(&driverVersion, "%s.driverVersion = %u\n", physicalDevicePropertiesName, physicalDeviceProperties.driverVersion);
	stprintf(&vendorID,      "%s.vendorID = %u\n", physicalDevicePropertiesName, physicalDeviceProperties.vendorID);
	stprintf(&deviceID,      "%s.deviceID = %u\n", physicalDevicePropertiesName, physicalDeviceProperties.deviceID);
	stprintf(&deviceType,    "%s.deviceType = %s\n", physicalDevicePropertiesName, vkuGetPhysicalDeviceTypeSignification(physicalDeviceProperties.deviceType));
	stprintf(&deviceName,    "%s.deviceName = %s\n", physicalDevicePropertiesName, physicalDeviceProperties.deviceName);
	char* pipelineCacheUUID[VK_UUID_SIZE] = {NULL};
	int pipelineCacheUUIDSize = 0;
	for(int i = 0; i < VK_UUID_SIZE; i++)
	{
		pipelineCacheUUIDSize+=stprintf(&(pipelineCacheUUID[i]), "%s.pipelineCacheUUID[%2i] = 0x%02hX\n", physicalDevicePropertiesName, i, physicalDeviceProperties.pipelineCacheUUID[i]);
	}

	length = stprintf(target, "%s%s%s%s%s%s", apiVersion, driverVersion, vendorID, deviceID, deviceType, deviceName);
	free(apiVersion);
	free(driverVersion);
	free(vendorID);
	free(deviceID);
	free(deviceType);
	free(deviceName);
	*target = (char*) realloc(*target, ((length+pipelineCacheUUIDSize)+1)*sizeof(char));
	(*target)[length] = '\0';
	for(int i = 0; i < VK_UUID_SIZE; i++)
	{
		strcat(*target, pipelineCacheUUID[i]);
		free(pipelineCacheUUID[i]);
	}
	length+=pipelineCacheUUIDSize;

	char *physicalDeviceLimitsName = (char*) malloc((strlen(physicalDevicePropertiesName) + 8) * sizeof(char));
	char *physicalSparsePropertiesName = (char*) malloc((strlen(physicalDevicePropertiesName) + 18) * sizeof(char));
	sprintf(physicalDeviceLimitsName, "%s.%s", physicalDevicePropertiesName, "limits");
	sprintf(physicalSparsePropertiesName, "%s.%s", physicalDevicePropertiesName, "sparseProperties");

	char *physicalDeviceLimitsPropertiesDescription = NULL;
	char *physicalDeviceSparsePropertiesDescription = NULL;
	int physicalDeviceLimitsPropertiesDescriptionSize;
	int physicalDeviceSparsePropertiesDescriptionSize;

	physicalDeviceLimitsPropertiesDescriptionSize = vkuDescribePhysicalDeviceLimitsPropertiesIntoString(physicalDeviceLimitsName, physicalDeviceProperties.limits, &physicalDeviceLimitsPropertiesDescription);
	physicalDeviceSparsePropertiesDescriptionSize = vkuDescribePhysicalDeviceSparsePropertiesIntoString(physicalSparsePropertiesName, physicalDeviceProperties.sparseProperties, &physicalDeviceSparsePropertiesDescription);

	free(physicalDeviceLimitsName);
	free(physicalSparsePropertiesName);

	*target = (char*) realloc(*target, (length + (physicalDeviceLimitsPropertiesDescriptionSize + physicalDeviceSparsePropertiesDescriptionSize) + 1)*sizeof(char));
	(*target)[length] = '\0';
	strcat(*target, physicalDeviceLimitsPropertiesDescription);
	strcat(*target, physicalDeviceSparsePropertiesDescription);

	free(physicalDeviceLimitsPropertiesDescription);
	free(physicalDeviceSparsePropertiesDescription);

	length+=(physicalDeviceLimitsPropertiesDescriptionSize + physicalDeviceSparsePropertiesDescriptionSize);

	return length;
}

int vkuDescribePhysicalDeviceFeaturesIntoString(const char *physicalDeviceFeaturesName, const VkPhysicalDeviceFeatures physicalDeviceFeatures, char **target)
{
	int length = 0;
	*target = NULL;

	char* robustBufferAccess = NULL;
	char* fullDrawIndexUint32 = NULL;
	char* imageCubeArray = NULL;
	char* independentBlend = NULL;
	char* geometryShader = NULL;
	char* tessellationShader = NULL;
	char* sampleRateShading = NULL;
	char* dualSrcBlend = NULL;
	char* logicOp = NULL;
	char* multiDrawIndirect = NULL;
	char* drawIndirectFirstInstance = NULL;
	char* depthClamp = NULL;
	char* depthBiasClamp = NULL;
	char* fillModeNonSolid = NULL;
	char* depthBounds = NULL;
	char* wideLines = NULL;
	char* largePoints = NULL;
	char* alphaToOne = NULL;
	char* multiViewport = NULL;
	char* samplerAnisotropy = NULL;
	char* textureCompressionETC2 = NULL;
	char* textureCompressionASTC_LDR = NULL;
	char* textureCompressionBC = NULL;
	char* occlusionQueryPrecise = NULL;
	char* pipelineStatisticsQuery = NULL;
	char* vertexPipelineStoresAndAtomics = NULL;
	char* fragmentStoresAndAtomics = NULL;
	char* shaderTessellationAndGeometryPointSize = NULL;
	char* shaderImageGatherExtended = NULL;
	char* shaderStorageImageExtendedFormats = NULL;
	char* shaderStorageImageMultisample = NULL;
	char* shaderStorageImageReadWithoutFormat = NULL;
	char* shaderStorageImageWriteWithoutFormat = NULL;
	char* shaderUniformBufferArrayDynamicIndexing = NULL;
	char* shaderSampledImageArrayDynamicIndexing = NULL;
	char* shaderStorageBufferArrayDynamicIndexing = NULL;
	char* shaderStorageImageArrayDynamicIndexing = NULL;
	char* shaderClipDistance = NULL;
	char* shaderCullDistance = NULL;
	char* shaderFloat64 = NULL;
	char* shaderInt64 = NULL;
	char* shaderInt16 = NULL;
	char* shaderResourceResidency = NULL;
	char* shaderResourceMinLod = NULL;
	char* sparseBinding = NULL;
	char* sparseResidencyBuffer = NULL;
	char* sparseResidencyImage2D = NULL;
	char* sparseResidencyImage3D = NULL;
	char* sparseResidency2Samples = NULL;
	char* sparseResidency4Samples = NULL;
	char* sparseResidency8Samples = NULL;
	char* sparseResidency16Samples = NULL;
	char* sparseResidencyAliased = NULL;
	char* variableMultisampleRate = NULL;
	char* inheritedQueries = NULL;

	stprintf(&robustBufferAccess, "%s.robustBufferAccess =                      %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.robustBufferAccess == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&fullDrawIndexUint32, "%s.fullDrawIndexUint32 =                     %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.fullDrawIndexUint32 == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&imageCubeArray, "%s.imageCubeArray =                          %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.imageCubeArray == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&independentBlend, "%s.independentBlend =                        %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.independentBlend == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&geometryShader, "%s.geometryShader =                          %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.geometryShader == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&tessellationShader, "%s.tessellationShader =                      %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.tessellationShader == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&sampleRateShading, "%s.sampleRateShading =                       %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sampleRateShading == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&dualSrcBlend, "%s.dualSrcBlend =                            %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.dualSrcBlend == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&logicOp, "%s.logicOp =                                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.logicOp == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&multiDrawIndirect, "%s.multiDrawIndirect =                       %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.multiDrawIndirect == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&drawIndirectFirstInstance, "%s.drawIndirectFirstInstance =               %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.drawIndirectFirstInstance == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&depthClamp, "%s.depthClamp =                              %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.depthClamp == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&depthBiasClamp, "%s.depthBiasClamp =                          %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.depthBiasClamp == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&fillModeNonSolid, "%s.fillModeNonSolid =                        %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.fillModeNonSolid == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&depthBounds, "%s.depthBounds =                             %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.depthBounds == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&wideLines, "%s.wideLines =                               %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.wideLines == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&largePoints, "%s.largePoints =                             %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.largePoints == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&alphaToOne, "%s.alphaToOne =                              %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.alphaToOne == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&multiViewport, "%s.multiViewport =                           %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.multiViewport == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&samplerAnisotropy, "%s.samplerAnisotropy =                       %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.samplerAnisotropy == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&textureCompressionETC2, "%s.textureCompressionETC2 =                  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.textureCompressionETC2 == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&textureCompressionASTC_LDR, "%s.textureCompressionASTC_LDR =              %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.textureCompressionASTC_LDR == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&textureCompressionBC, "%s.textureCompressionBC =                    %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.textureCompressionBC == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&occlusionQueryPrecise, "%s.occlusionQueryPrecise =                   %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.occlusionQueryPrecise == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&pipelineStatisticsQuery, "%s.pipelineStatisticsQuery =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.pipelineStatisticsQuery == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&vertexPipelineStoresAndAtomics, "%s.vertexPipelineStoresAndAtomics =          %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.vertexPipelineStoresAndAtomics == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&fragmentStoresAndAtomics, "%s.fragmentStoresAndAtomics =                %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.fragmentStoresAndAtomics == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderTessellationAndGeometryPointSize, "%s.shaderTessellationAndGeometryPointSize =  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderTessellationAndGeometryPointSize == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderImageGatherExtended, "%s.shaderImageGatherExtended =               %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderImageGatherExtended == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderStorageImageExtendedFormats, "%s.shaderStorageImageExtendedFormats =       %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageImageExtendedFormats == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderStorageImageMultisample, "%s.shaderStorageImageMultisample =           %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageImageMultisample == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderStorageImageReadWithoutFormat, "%s.shaderStorageImageReadWithoutFormat =     %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageImageReadWithoutFormat == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderStorageImageWriteWithoutFormat, "%s.shaderStorageImageWriteWithoutFormat =    %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageImageWriteWithoutFormat == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderUniformBufferArrayDynamicIndexing, "%s.shaderUniformBufferArrayDynamicIndexing = %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderUniformBufferArrayDynamicIndexing == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderSampledImageArrayDynamicIndexing, "%s.shaderSampledImageArrayDynamicIndexing =  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderSampledImageArrayDynamicIndexing == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderStorageBufferArrayDynamicIndexing, "%s.shaderStorageBufferArrayDynamicIndexing = %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageBufferArrayDynamicIndexing == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderStorageImageArrayDynamicIndexing, "%s.shaderStorageImageArrayDynamicIndexing =  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderStorageImageArrayDynamicIndexing == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderClipDistance, "%s.shaderClipDistance =                      %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderClipDistance == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderCullDistance, "%s.shaderCullDistance =                      %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderCullDistance == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderFloat64, "%s.shaderFloat64 =                           %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderFloat64 == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderInt64, "%s.shaderInt64 =                             %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderInt64 == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderInt16, "%s.shaderInt16 =                             %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderInt16 == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderResourceResidency, "%s.shaderResourceResidency =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderResourceResidency == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&shaderResourceMinLod, "%s.shaderResourceMinLod =                    %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.shaderResourceMinLod == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&sparseBinding, "%s.sparseBinding =                           %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseBinding == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&sparseResidencyBuffer, "%s.sparseResidencyBuffer =                   %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidencyBuffer == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&sparseResidencyImage2D, "%s.sparseResidencyImage2D =                  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidencyImage2D == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&sparseResidencyImage3D, "%s.sparseResidencyImage3D =                  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidencyImage3D == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&sparseResidency2Samples, "%s.sparseResidency2Samples =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidency2Samples == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&sparseResidency4Samples, "%s.sparseResidency4Samples =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidency4Samples == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&sparseResidency8Samples, "%s.sparseResidency8Samples =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidency8Samples == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&sparseResidency16Samples, "%s.sparseResidency16Samples =                %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidency16Samples == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&sparseResidencyAliased, "%s.sparseResidencyAliased =                  %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.sparseResidencyAliased == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&variableMultisampleRate, "%s.variableMultisampleRate =                 %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.variableMultisampleRate == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));
	stprintf(&inheritedQueries, "%s.inheritedQueries =                        %s\n", physicalDeviceFeaturesName, ((physicalDeviceFeatures.inheritedQueries == VK_TRUE) ? "VK_TRUE" : "VK_FALSE"));

	length = stprintf(target, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", robustBufferAccess, fullDrawIndexUint32, imageCubeArray, independentBlend, geometryShader, tessellationShader, sampleRateShading, dualSrcBlend, logicOp, multiDrawIndirect, drawIndirectFirstInstance, depthClamp, depthBiasClamp, fillModeNonSolid, depthBounds, wideLines, largePoints, alphaToOne, multiViewport, samplerAnisotropy, textureCompressionETC2, textureCompressionASTC_LDR, textureCompressionBC, occlusionQueryPrecise, pipelineStatisticsQuery, vertexPipelineStoresAndAtomics, fragmentStoresAndAtomics, shaderTessellationAndGeometryPointSize, shaderImageGatherExtended, shaderStorageImageExtendedFormats, shaderStorageImageMultisample, shaderStorageImageReadWithoutFormat, shaderStorageImageWriteWithoutFormat, shaderUniformBufferArrayDynamicIndexing, shaderSampledImageArrayDynamicIndexing, shaderStorageBufferArrayDynamicIndexing, shaderStorageImageArrayDynamicIndexing, shaderClipDistance, shaderCullDistance, shaderFloat64, shaderInt64, shaderInt16, shaderResourceResidency, shaderResourceMinLod, sparseBinding, sparseResidencyBuffer, sparseResidencyImage2D, sparseResidencyImage3D, sparseResidency2Samples, sparseResidency4Samples, sparseResidency8Samples, sparseResidency16Samples, sparseResidencyAliased, variableMultisampleRate, inheritedQueries);

	free(robustBufferAccess);
	free(fullDrawIndexUint32);
	free(imageCubeArray);
	free(independentBlend);
	free(geometryShader);
	free(tessellationShader);
	free(sampleRateShading);
	free(dualSrcBlend);
	free(logicOp);
	free(multiDrawIndirect);
	free(drawIndirectFirstInstance);
	free(depthClamp);
	free(depthBiasClamp);
	free(fillModeNonSolid);
	free(depthBounds);
	free(wideLines);
	free(largePoints);
	free(alphaToOne);
	free(multiViewport);
	free(samplerAnisotropy);
	free(textureCompressionETC2);
	free(textureCompressionASTC_LDR);
	free(textureCompressionBC);
	free(occlusionQueryPrecise);
	free(pipelineStatisticsQuery);
	free(vertexPipelineStoresAndAtomics);
	free(fragmentStoresAndAtomics);
	free(shaderTessellationAndGeometryPointSize);
	free(shaderImageGatherExtended);
	free(shaderStorageImageExtendedFormats);
	free(shaderStorageImageMultisample);
	free(shaderStorageImageReadWithoutFormat);
	free(shaderStorageImageWriteWithoutFormat);
	free(shaderUniformBufferArrayDynamicIndexing);
	free(shaderSampledImageArrayDynamicIndexing);
	free(shaderStorageBufferArrayDynamicIndexing);
	free(shaderStorageImageArrayDynamicIndexing);
	free(shaderClipDistance);
	free(shaderCullDistance);
	free(shaderFloat64);
	free(shaderInt64);
	free(shaderInt16);
	free(shaderResourceResidency);
	free(shaderResourceMinLod);
	free(sparseBinding);
	free(sparseResidencyBuffer);
	free(sparseResidencyImage2D);
	free(sparseResidencyImage3D);
	free(sparseResidency2Samples);
	free(sparseResidency4Samples);
	free(sparseResidency8Samples);
	free(sparseResidency16Samples);
	free(sparseResidencyAliased);
	free(variableMultisampleRate);
	free(inheritedQueries);

	return length;
}

int vkuDescribeMemoryTypeIntoString(const char *memoryTypeName, const VkMemoryType memoryType, char **target)
{
	int length = 0;
	*target = NULL;

	char* propertyFlags = NULL;
	char* propertyFlagsDescription = NULL;
	char* heapIndex = NULL;

	stprintf(&propertyFlags, "%s.propertyFlags = ", memoryTypeName);
	vkuDescribeMemoryPropertyFlagsIntoString(memoryType.propertyFlags, &propertyFlagsDescription);
	stprintf(&heapIndex, "%s.heapIndex = %u\n", memoryTypeName, memoryType.heapIndex);

	length = stprintf(target, "%s%s%s", propertyFlags, propertyFlagsDescription, heapIndex);

	free(propertyFlags);
	free(propertyFlagsDescription);
	free(heapIndex);

	return length;
}

int vkuDescribeMemoryHeapIntoString(const char *memoryHeapName, const VkMemoryHeap memoryHeap, char **target)
{
	int length = 0;
	*target = NULL;

	char* flags = NULL;
	char* flagsDescription = NULL;
	char* size = NULL;

	stprintf(&flags, "%s.flags = ", memoryHeapName);
	vkuDescribeMemoryHeapFlagsIntoString(memoryHeap.flags, &flagsDescription);
	stprintf(&size, "%s.size = %lu\n", memoryHeapName, memoryHeap.size);

	length = stprintf(target, "%s%s%s", flags, flagsDescription, size);

	free(flags);
	free(flagsDescription);
	free(size);

	return length;
}

int vkuDescribePhysicalDeviceMemoryPropertiesIntoString(const char *physicalDeviceMemoryPropertiesName, const VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties, char **target)
{
	int length = 0;
	*target = NULL;

	char* memoryTypeCount;
	stprintf(&memoryTypeCount, "%s.memoryTypeCount = %u\n", physicalDeviceMemoryPropertiesName, physicalDeviceMemoryProperties.memoryTypeCount);

	char* physicalDeviceMemoryPropertiesMemoryTypes = NULL;
	int physicalDeviceMemoryPropertiesMemoryTypesSize = 0;

	unsigned int i = 0;
	char *physicalDeviceMemoryPropertiesMemoryTypeName = (char*) malloc((strlen(physicalDeviceMemoryPropertiesName) + 47) * sizeof(char));
	for(i = 0; i < physicalDeviceMemoryProperties.memoryTypeCount; i++)
	{
		sprintf(physicalDeviceMemoryPropertiesMemoryTypeName, "%s.%s[%i]", physicalDeviceMemoryPropertiesName, "memory_type", i);

		char* physicalDeviceMemoryPropertiesMemoryType = NULL;
		int physicalDeviceMemoryPropertiesMemoryTypeSize = vkuDescribeMemoryTypeIntoString(physicalDeviceMemoryPropertiesMemoryTypeName, physicalDeviceMemoryProperties.memoryTypes[i], &physicalDeviceMemoryPropertiesMemoryType);

		physicalDeviceMemoryPropertiesMemoryTypes = (char*) realloc(physicalDeviceMemoryPropertiesMemoryTypes, ((physicalDeviceMemoryPropertiesMemoryTypesSize+physicalDeviceMemoryPropertiesMemoryTypeSize)+1)*sizeof(char));
		physicalDeviceMemoryPropertiesMemoryTypes[physicalDeviceMemoryPropertiesMemoryTypesSize] = '\0';
		strcat(physicalDeviceMemoryPropertiesMemoryTypes, physicalDeviceMemoryPropertiesMemoryType);
		physicalDeviceMemoryPropertiesMemoryTypesSize+=physicalDeviceMemoryPropertiesMemoryTypeSize;
		free(physicalDeviceMemoryPropertiesMemoryType);
	}
	free(physicalDeviceMemoryPropertiesMemoryTypeName);



	char* memoryHeapCount;
	stprintf(&memoryHeapCount, "%s.memoryHeapCount = %u\n", physicalDeviceMemoryPropertiesName, physicalDeviceMemoryProperties.memoryHeapCount);

	char* physicalDeviceMemoryPropertiesMemoryHeaps = NULL;
	int physicalDeviceMemoryPropertiesMemoryHeapsSize = 0;

	char *physicalDeviceMemoryPropertiesMemoryHeapName = (char*) malloc((strlen(physicalDeviceMemoryPropertiesName) + 47) * sizeof(char));
	for(i = 0; i < physicalDeviceMemoryProperties.memoryHeapCount; i++)
	{
		sprintf(physicalDeviceMemoryPropertiesMemoryHeapName, "%s.%s[%i]", physicalDeviceMemoryPropertiesName, "memory_heap", i);

		char* physicalDeviceMemoryPropertiesMemoryHeap = NULL;
		int physicalDeviceMemoryPropertiesMemoryHeapSize = vkuDescribeMemoryHeapIntoString(physicalDeviceMemoryPropertiesMemoryHeapName, physicalDeviceMemoryProperties.memoryHeaps[i], &physicalDeviceMemoryPropertiesMemoryHeap);

		physicalDeviceMemoryPropertiesMemoryHeaps = (char*) realloc(physicalDeviceMemoryPropertiesMemoryHeaps, ((physicalDeviceMemoryPropertiesMemoryHeapsSize+physicalDeviceMemoryPropertiesMemoryHeapSize)+1)*sizeof(char));
		physicalDeviceMemoryPropertiesMemoryHeaps[physicalDeviceMemoryPropertiesMemoryHeapsSize] = '\0';
		strcat(physicalDeviceMemoryPropertiesMemoryHeaps, physicalDeviceMemoryPropertiesMemoryHeap);
		physicalDeviceMemoryPropertiesMemoryHeapsSize+=physicalDeviceMemoryPropertiesMemoryHeapSize;
		free(physicalDeviceMemoryPropertiesMemoryHeap);
	}
	free(physicalDeviceMemoryPropertiesMemoryHeapName);

	length = stprintf(target, "%s%s%s%s", memoryTypeCount, physicalDeviceMemoryPropertiesMemoryTypes, memoryHeapCount, physicalDeviceMemoryPropertiesMemoryHeaps);

	free(memoryTypeCount);
	free(physicalDeviceMemoryPropertiesMemoryTypes);
	free(memoryHeapCount);
	free(physicalDeviceMemoryPropertiesMemoryHeaps);

	return length;
}

int vkuDescribeExtent3DIntoString(const char *extent3DName, VkExtent3D extent3D, char **target)
{
	int length = 0;
	*target = NULL;

	char* width = NULL;
	char* height = NULL;
	char* depth = NULL;

	stprintf(&width, "%s.width =  %u\n", extent3DName, extent3D.width);
	stprintf(&height, "%s.height = %u\n", extent3DName, extent3D.height);
	stprintf(&depth, "%s.depth =  %u\n", extent3DName, extent3D.depth);

	length = stprintf(target, "%s%s%s", width, height, depth);

	free(width);
	free(height);
	free(depth);

	return length;
}

int vkuDescribeQueueFamilyPropertiesIntoString(const char *queueFamilyPropertiesName, const VkQueueFamilyProperties queueFamilyProperties, char **target)
{
	int length = 0;
	*target = NULL;

	char* queueFlags = NULL;
	char* queueFlagsDescription = NULL;
	char* queueCount = NULL;
	char* timestampValidBits = NULL;
	char* extent3DDescription = NULL;

	stprintf(&queueFlags, "%s.queueFlags = ", queueFamilyPropertiesName);
	vkuDescribeQueueFlagsIntoString(queueFamilyProperties.queueFlags, &queueFlagsDescription);
	stprintf(&queueCount, "%s.queueCount = %u\n", queueFamilyPropertiesName, queueFamilyProperties.queueCount);
	stprintf(&timestampValidBits, "%s.timestampValidBits = %u\n", queueFamilyPropertiesName, queueFamilyProperties.timestampValidBits);

	char *extent3DName = (char*) malloc((strlen(queueFamilyPropertiesName) + 29) * sizeof(char));
	sprintf(extent3DName, "%s.%s", queueFamilyPropertiesName, "minImageTransferGranularity");
	vkuDescribeExtent3DIntoString(extent3DName, queueFamilyProperties.minImageTransferGranularity, &extent3DDescription);
	free(extent3DName);

	length = stprintf(target, "%s%s%s%s%s", queueFlags, queueFlagsDescription, queueCount, timestampValidBits, extent3DDescription);

	free(queueFlags);
	free(queueFlagsDescription);
	free(queueCount);
	free(timestampValidBits);
	free(extent3DDescription);

	return length;
}

int vkuDescribeQueuesFamilyPropertiesIntoString(const char *physicalDeviceName, const uint32_t queueFamilyPropertiesCount, const VkQueueFamilyProperties *queueFamilyProperties, char **target)
{
	int length = 0;
	*target = NULL;

	for(unsigned int i = 0; i < queueFamilyPropertiesCount; i++)
	{
		const char queueFamilyPropertiesLabel[] = "queue_family_properties_";
		char *queueFamilyPropertiesName = (char*) malloc((strlen(physicalDeviceName) + strlen(physicalDeviceName) + 4 + 32) * sizeof(char));
		sprintf(queueFamilyPropertiesName, "%s.%s(%i)", physicalDeviceName, queueFamilyPropertiesLabel, i);

		char* queueFamilyPropertieDescription = NULL;
		int queueFamilyPropertieDescriptionSize = vkuDescribeQueueFamilyPropertiesIntoString(queueFamilyPropertiesName, queueFamilyProperties[i], &queueFamilyPropertieDescription);

		*target = (char*) realloc(*target, (length+(queueFamilyPropertieDescriptionSize)+1)*sizeof(char));
		(*target)[0] = '\0';
		strcat(*target, queueFamilyPropertieDescription);
		length+=queueFamilyPropertieDescriptionSize;
		free(queueFamilyPropertieDescription);
		free(queueFamilyPropertiesName);
	}

	return length;
}

int vkuDescribePhysicalDeviceIntoString(const char *physicalDeviceName, const VkPhysicalDevice physicalDevice, char **target)
{
	int length = 0;
	*target = NULL;

	VkPhysicalDeviceProperties physicalDeviceProperties;
	vkGetPhysicalDeviceProperties(physicalDevice, &physicalDeviceProperties);
	char *physicalDevicePropertiesName = (char*) malloc((strlen(physicalDeviceName) + 12) * sizeof(char));
	sprintf(physicalDevicePropertiesName, "%s.%s", physicalDeviceName, "properties");

	char *physicalDevicePropertiesDescription = NULL;
	vkuDescribePhysicalDevicePropertiesIntoString(physicalDevicePropertiesName, physicalDeviceProperties, &physicalDevicePropertiesDescription);
	free(physicalDevicePropertiesName);

	VkPhysicalDeviceFeatures physicalDeviceFeatures;
	vkGetPhysicalDeviceFeatures(physicalDevice, &physicalDeviceFeatures);
	char *physicalDeviceFeaturesName = (char*) malloc((strlen(physicalDeviceName) + 20) * sizeof(char));
	sprintf(physicalDeviceFeaturesName, "%s.%s", physicalDeviceName, "available_features");

	char *physicalDeviceFeaturesDescription = NULL;
	vkuDescribePhysicalDeviceFeaturesIntoString(physicalDeviceFeaturesName, physicalDeviceFeatures, &physicalDeviceFeaturesDescription);
	free(physicalDeviceFeaturesName);

	VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &physicalDeviceMemoryProperties);
	char *physicalDeviceMemoryPropertiesName = (char*) malloc((strlen(physicalDeviceName) + 19) * sizeof(char));
	sprintf(physicalDeviceMemoryPropertiesName, "%s.%s", physicalDeviceName, "memory_properties");

	char *physicalDeviceMemoryDescription = NULL;
	vkuDescribePhysicalDeviceMemoryPropertiesIntoString(physicalDeviceMemoryPropertiesName, physicalDeviceMemoryProperties, &physicalDeviceMemoryDescription);
	free(physicalDeviceMemoryPropertiesName);


	length+=stprintf(target, "%s%s%s", physicalDevicePropertiesDescription, physicalDeviceFeaturesDescription, physicalDeviceMemoryDescription);
	free(physicalDevicePropertiesDescription);
	free(physicalDeviceFeaturesDescription);
	free(physicalDeviceMemoryDescription);


	uint32_t queueFamilyPropertiesCount = 0;
	VkQueueFamilyProperties *queueFamilyProperties = NULL;
	vkuGetQueueFamilyProperties(&physicalDevice, &queueFamilyPropertiesCount, &queueFamilyProperties);
	if(queueFamilyPropertiesCount > 0)
	{
		char *queuesFamilyPropertiesDescription = NULL;
		int queuesFamilyPropertiesDescriptionSize = vkuDescribeQueuesFamilyPropertiesIntoString(physicalDeviceName, queueFamilyPropertiesCount, queueFamilyProperties, &queuesFamilyPropertiesDescription);
		*target = (char*) realloc(*target, (length+(queuesFamilyPropertiesDescriptionSize)+1)*sizeof(char));
		(*target)[length] = '\0';
		strcat(*target, queuesFamilyPropertiesDescription);
		length+=queuesFamilyPropertiesDescriptionSize;
		free(queuesFamilyPropertiesDescription);
		free(queueFamilyProperties);
	}

	uint32_t layerPropertiesCount = 0;
	VkLayerProperties *layerProperties = NULL;
	vkuGetDeviceLayerProperties(&physicalDevice, &layerPropertiesCount, &layerProperties);
	if(layerPropertiesCount > 0)
	{
		char *layerPropertiesDescription = NULL;
		int layerPropertiesDescriptionSize = vkuDescribeLayerPropertiesIntoString(physicalDeviceName, layerPropertiesCount, layerProperties, &layerPropertiesDescription);
		*target = (char*) realloc(*target, (length+(layerPropertiesDescriptionSize)+1)*sizeof(char));
		(*target)[length] = '\0';
		strcat(*target, layerPropertiesDescription);
		length+=layerPropertiesDescriptionSize;
		free(layerPropertiesDescription);
		free(layerProperties);
	}

	uint32_t extensionPropertiesCount = 0;
	VkExtensionProperties *extensionProperties = NULL;
	vkuGetDeviceExtensionProperties(&physicalDevice, &extensionPropertiesCount, &extensionProperties);
	if(extensionPropertiesCount > 0)
	{
		char *extensionPropertiesDescription = NULL;
		int extensionPropertiesDescriptionSize = vkuDescribeExtensionPropertiesIntoString(physicalDeviceName, extensionPropertiesCount, extensionProperties, &extensionPropertiesDescription);
		*target = (char*) realloc(*target, (length+(extensionPropertiesDescriptionSize)+1)*sizeof(char));
		(*target)[length] = '\0';
		strcat(*target, extensionPropertiesDescription);
		length+=extensionPropertiesDescriptionSize;
		free(extensionPropertiesDescription);
		free(extensionProperties);
	}

	return length;
}

int vkuDescribePhysicalDevicesIntoString(const uint32_t physicalDevicesCount, const VkPhysicalDevice *physicalDevices, char **target)
{
	int length = 0;
	*target = NULL;
	for(unsigned int i = 0; i < physicalDevicesCount; i++)
	{
		const char physicalDeviceLabel[] = "physical_device_";
		char *physicalDeviceName = (char*) malloc((strlen(physicalDeviceLabel) + 3 + 32) * sizeof(char));
		sprintf(physicalDeviceName, "%s(%i)", physicalDeviceLabel, i);
		int physicalDeviceNameSize = strlen(physicalDeviceName);

		*target = (char*) realloc(*target, (length+(physicalDeviceNameSize)+3)*sizeof(char));
		(*target)[length]='\0';
		strcat(*target, physicalDeviceName);
		strcat(*target, ":\n");
		length+=(physicalDeviceNameSize+2);

		//vkuDescribePhysicalDevice(physicalDeviceName, physicalDevices[i]);

		char *physicalDeviceDescription = NULL;
		int physicalDeviceDescriptionSize = 0;

		physicalDeviceDescriptionSize = vkuDescribePhysicalDeviceIntoString(physicalDeviceName, physicalDevices[i], &physicalDeviceDescription);
		if(physicalDeviceDescriptionSize < 0)
		{
			if(*target !=NULL)
			{
				free(*target);
				*target = NULL;
			}
			free(physicalDeviceName);
			return physicalDeviceDescriptionSize;
		}
		else if(physicalDeviceDescriptionSize > 0)
		{
			*target = (char*) realloc(*target, (length+(physicalDeviceDescriptionSize)+1)*sizeof(char));
			(*target)[length]='\0';

			strcat(*target, physicalDeviceDescription);
			free(physicalDeviceDescription);

			length+=(physicalDeviceDescriptionSize);
		}

		free(physicalDeviceName);
	}
	return length;
}

int vkuListPhysicalDevicesNamesIntoString(const uint32_t physicalDevicesCount, const VkPhysicalDevice *physicalDevices, char **target)
{
	int length = 0;
	*target = NULL;
	for(unsigned int i = 0; i < physicalDevicesCount; i++)
	{
		const char physicalDeviceLabel[] = "physical_device_";
		char *physicalDeviceName = (char*) malloc((strlen(physicalDeviceLabel) + 3 + 32) * sizeof(char));
		sprintf(physicalDeviceName, "%s(%i)", physicalDeviceLabel, i);

		char *deviceName;
		int deviceNameSize;

		VkPhysicalDeviceProperties physicalDeviceProperties;
		vkGetPhysicalDeviceProperties(physicalDevices[i], &physicalDeviceProperties);
		deviceNameSize = stprintf(&deviceName, "%s : %s\n", physicalDeviceName, physicalDeviceProperties.deviceName);
		if(deviceNameSize < 0)
		{
			*target = NULL;
			free(physicalDeviceName);
			return deviceNameSize;
		}

		free(physicalDeviceName);
		*target = (char*) realloc(*target, (length+(deviceNameSize)+1)*sizeof(char));
		(*target)[length] = '\0';

		strcat(*target, deviceName);

		free(deviceName);

		length+=(deviceNameSize);
	}
	return length;
}

int vkuDescribeLayerPropertiesIntoString(const char *physicalDeviceName, const uint32_t layerPropertiesCount, const VkLayerProperties *layerProperties, char **target)
{
	int length = 0;
	*target = NULL;
	for(unsigned int i = 0; i < layerPropertiesCount; i++)
	{
		const char *localPhysicalDeviceName = "";
		int localPhysicalDeviceNameSize = 0;
		if(physicalDeviceName != NULL)
		{
			localPhysicalDeviceName = physicalDeviceName;
			localPhysicalDeviceNameSize = strlen(physicalDeviceName);
		}

		const char layerPropertiesLabel[] = "layer_properties_";

		char *layerPropertiesName = NULL;
		if(localPhysicalDeviceNameSize > 0)
		{
			layerPropertiesName = (char*) malloc((localPhysicalDeviceNameSize + 1 + strlen(layerPropertiesLabel) + 3 + 32) * sizeof(char));
			sprintf(layerPropertiesName, "%s.%s(%i)", localPhysicalDeviceName, layerPropertiesLabel, i);
		}
		else
		{
			layerPropertiesName = (char*) malloc((strlen(layerPropertiesLabel) + 3 + 32) * sizeof(char));
			sprintf(layerPropertiesName, "%s(%i)", layerPropertiesLabel, i);
		}

		char *layerName = NULL;
		char *specVersion = NULL;
		char *implementationVersion = NULL;
		char *description = NULL;
		int layerNameSize;
		int specVersionSize;
		int implementationVersionSize;
		int descriptionSize;

		layerNameSize = stprintf(&layerName, "%s.layerName = %s\n", layerPropertiesName, layerProperties[i].layerName);
		if(layerNameSize < 0)
		{
			*target = NULL;
			free(layerPropertiesName);
			return layerNameSize;
		}
		specVersionSize = stprintf(&specVersion, "%s.specVersion = %u\n", layerPropertiesName, layerProperties[i].specVersion);
		if(specVersionSize < 0)
		{
			*target = NULL;
			free(layerName);
			free(layerPropertiesName);
			return layerNameSize;
		}
		implementationVersionSize = stprintf(&implementationVersion, "%s.implementationVersion = %u\n", layerPropertiesName, layerProperties[i].implementationVersion);
		if(implementationVersionSize < 0)
		{
			*target = NULL;
			free(layerName);
			free(specVersion);
			free(layerPropertiesName);
			return layerNameSize;
		}
		descriptionSize = stprintf(&description, "%s.description = %s\n", layerPropertiesName, layerProperties[i].description);
		if(descriptionSize < 0)
		{
			*target = NULL;
			free(layerName);
			free(specVersion);
			free(implementationVersion);
			free(layerPropertiesName);
			return layerNameSize;
		}

		free(layerPropertiesName);
		*target = (char*) realloc(*target, (length+(layerNameSize + specVersionSize +implementationVersionSize + descriptionSize)+1)*sizeof(char));
		(*target)[length] = '\0';

		strcat(*target, layerName);
		strcat(*target, specVersion);
		strcat(*target, implementationVersion);
		strcat(*target, description);

		free(description);
		free(implementationVersion);
		free(specVersion);
		free(layerName);

		length+=(layerNameSize + specVersionSize +implementationVersionSize + descriptionSize);
	}
	return length;
}

int vkuListLayerPropertiesNamesIntoString(const uint32_t layerPropertiesCount, const VkLayerProperties *layerProperties, char **target)
{
	int length = 0;
	*target = NULL;
	for(unsigned int i = 0; i < layerPropertiesCount; i++)
	{
		const char layerPropertiesLabel[] = "instance.layer_properties_";

		char *layerPropertiesName = (char*) malloc((strlen(layerPropertiesLabel) + 3 + 32) * sizeof(char));
		sprintf(layerPropertiesName, "%s(%i)", layerPropertiesLabel, i);

		char *layerName = NULL;
		int layerNameSize;

		layerNameSize = stprintf(&layerName, "%s.layerName = %s\n", layerPropertiesName, layerProperties[i].layerName);
		if(layerNameSize < 0)
		{
			*target = NULL;
			free(layerPropertiesName);
			return layerNameSize;
		}

		free(layerPropertiesName);

		*target = (char*) realloc(*target, (length+(layerNameSize)+1)*sizeof(char));
		(*target)[length] = '\0';

		strcat(*target, layerName);

		free(layerName);

		length+=(layerNameSize);
	}
	return length;
}

int vkuDescribeExtensionPropertiesIntoString(const char *physicalDeviceName, const uint32_t extensionPropertiesCount, const VkExtensionProperties *extensionProperties, char **target)
{
	int length = 0;
	*target = NULL;
	for(unsigned int i = 0; i < extensionPropertiesCount; i++)
	{
		const char *localExtensionDeviceName = "";
		int localPhysicalDeviceNameSize = 0;
		if(physicalDeviceName != NULL)
		{
			localExtensionDeviceName = physicalDeviceName;
			localPhysicalDeviceNameSize = strlen(physicalDeviceName);
		}

		const char extensionPropertiesLabel[] = "extension_properties_";

		char *extensionPropertiesName;
		if(localPhysicalDeviceNameSize > 0)
		{
			extensionPropertiesName = (char*) malloc((localPhysicalDeviceNameSize + 1 + strlen(extensionPropertiesLabel) + 3 + 32) * sizeof(char));
			sprintf(extensionPropertiesName, "%s.%s(%i)", localExtensionDeviceName, extensionPropertiesLabel, i);
		}
		else
		{
			extensionPropertiesName = (char*) malloc((strlen(extensionPropertiesLabel) + 3 + 32) * sizeof(char));
			sprintf(extensionPropertiesName, "%s(%i)", extensionPropertiesLabel, i);
		}

		char *extensionName = NULL;
		char *specVersion = NULL;
		int extensionNameSize = 0;
		int specVersionSize = 0;

		extensionNameSize = stprintf(&extensionName, "%s.extensionName = %s\n", extensionPropertiesName, extensionProperties[i].extensionName);
		if(extensionNameSize < 0)
		{
			if(*target !=NULL)
			{
				free(*target);
				*target = NULL;
			}
			free(extensionPropertiesName);
			return extensionNameSize;
		}
		specVersionSize = stprintf(&specVersion, "%s.specVersion = %u\n", extensionPropertiesName, extensionProperties[i].specVersion);
		if(specVersionSize < 0)
		{
			if(*target !=NULL)
			{
				free(*target);
				*target = NULL;
			}
			free(extensionName);
			free(extensionPropertiesName);
			return specVersionSize;
		}

		free(extensionPropertiesName);
		*target = (char*) realloc(*target, (length+(extensionNameSize + specVersionSize)+1)*sizeof(char));
		(*target)[length] = '\0';
		strcat(*target, extensionName);
		strcat(*target, specVersion);
		free(specVersion);
		free(extensionName);

		length+=((extensionNameSize + specVersionSize));
	}
	return length;
}

