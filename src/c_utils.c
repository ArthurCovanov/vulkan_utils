/*************************************/
/* Name        : c_utils.c           */
/* Author      : Arthur Covanov      */
/* Version     : 0.1                 */
/* Copyright   : GPL                 */
/* Description : Usefull tools for C */
/*************************************/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "c_utils.h"

int stprintf(char ** __restrict__ _Dest, const char * __restrict__ _Format, ...)
{
	va_list arg;
	int size = 0;
	int done = 0;
	char *result = NULL;

	va_start (arg, _Format);
	size = vsnprintf(NULL, size, _Format, arg);
	va_end (arg);

	if(size < 0)
		return size;

	result = (char*) malloc((size+1)*sizeof(char));
	result[0]='\0';

	va_start (arg, _Format);
	done = vsprintf(result, _Format, arg);
	va_end (arg);

	if(done < 0 || done != size)
	{
		free(result);
		result = NULL;
		return done;
	}

	result[done] = '\0';

	*_Dest = result;
	return done;
}

