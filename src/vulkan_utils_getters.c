/***********************************************************************/
/* Name        : vulkan_utils_getters.c                                */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#include <vulkan/vulkan.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vulkan_utils.h"

void vkuGetQueueFamilyProperties(const VkPhysicalDevice *physicalDevice, uint32_t *queueFamilyPropertiesCount, VkQueueFamilyProperties **queueFamilyProperties)
{
	vkGetPhysicalDeviceQueueFamilyProperties(*physicalDevice, queueFamilyPropertiesCount, NULL);
	*queueFamilyProperties = (VkQueueFamilyProperties*) malloc((*queueFamilyPropertiesCount) * sizeof(VkQueueFamilyProperties));
	vkGetPhysicalDeviceQueueFamilyProperties(*physicalDevice, queueFamilyPropertiesCount, *queueFamilyProperties);
}

VkResult vkuGetPhysicalDevices(const VkInstance *instance, uint32_t *physicalDevicesCount, VkPhysicalDevice **physicalDevices)
{
	VkResult result; //<- Will contains results of Vulkan functions
	result = vkEnumeratePhysicalDevices(*instance, physicalDevicesCount, NULL);
	if(result != VK_SUCCESS)
		return result;

	*physicalDevices = (VkPhysicalDevice*) malloc((*physicalDevicesCount) * sizeof(VkPhysicalDevice));
	result = vkEnumeratePhysicalDevices(*instance, physicalDevicesCount, *physicalDevices);
	if(result != VK_SUCCESS)
		free(*physicalDevices);
	return result;
}

VkResult vkuGetDeviceLayerProperties(const VkPhysicalDevice *physicalDevice, uint32_t *layerPropertiesCount, VkLayerProperties **layerProperties)
{
	VkResult result; //<- Will contains results of Vulkan functions
	result = vkEnumerateDeviceLayerProperties(*physicalDevice, layerPropertiesCount, NULL);
	if(result != VK_SUCCESS)
		return result;
	if(layerPropertiesCount > 0)
	{
		*layerProperties = (VkLayerProperties*) malloc((*layerPropertiesCount) * sizeof(VkLayerProperties));
		result = vkEnumerateDeviceLayerProperties(*physicalDevice, layerPropertiesCount, *layerProperties);
		if(result != VK_SUCCESS)
			free(*layerProperties);
	}
	return result;
}

VkResult vkuGetDeviceExtensionProperties(const VkPhysicalDevice *physicalDevice, uint32_t *extensionPropertiesCount, VkExtensionProperties **extensionProperties)
{
	VkResult result; //<- Will contains results of Vulkan functions
	result = vkEnumerateDeviceExtensionProperties(*physicalDevice, NULL, extensionPropertiesCount, NULL);
	if(result != VK_SUCCESS)
		return result;
	if(extensionPropertiesCount > 0)
	{
		*extensionProperties = (VkExtensionProperties*) malloc((*extensionPropertiesCount) * sizeof(VkExtensionProperties));
		result = vkEnumerateDeviceExtensionProperties(*physicalDevice, NULL, extensionPropertiesCount, *extensionProperties);
		if(result != VK_SUCCESS)
			free(*extensionProperties);
	}
	return result;
}

VkResult vkuGetInstanceLayerProperties(uint32_t *layerPropertiesCount, VkLayerProperties **layerProperties)
{
	VkResult result; //<- Will contains results of Vulkan functions
	result = vkEnumerateInstanceLayerProperties(layerPropertiesCount, NULL);
	if(result != VK_SUCCESS)
		return result;
	if(layerPropertiesCount > 0)
	{
		*layerProperties = (VkLayerProperties*) malloc((*layerPropertiesCount) * sizeof(VkLayerProperties));
		result = vkEnumerateInstanceLayerProperties(layerPropertiesCount, *layerProperties);
		if(result != VK_SUCCESS)
			free(*layerProperties);
	}
	return result;
}

VkResult vkuGetInstanceExtensionProperties(uint32_t *extensionPropertiesCount, VkExtensionProperties **extensionProperties)
{
	VkResult result; //<- Will contains results of Vulkan functions

	result = vkEnumerateInstanceExtensionProperties(NULL, extensionPropertiesCount, NULL);

	if(result != VK_SUCCESS)
		return result;
	if(extensionPropertiesCount > 0)
	{
		*extensionProperties = (VkExtensionProperties*) malloc((*extensionPropertiesCount) * sizeof(VkExtensionProperties));
		result = vkEnumerateInstanceExtensionProperties(NULL, extensionPropertiesCount, *extensionProperties);
		if(result != VK_SUCCESS)
			free(*extensionProperties);
	}
	return result;
}
