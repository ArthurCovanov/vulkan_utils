/***********************************************************************/
/* Name        : vulkan_utils_significations.c                         */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#include <vulkan/vulkan.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vulkan_utils.h"

const char* vkuGetResultSignification(const VkResult result)
{
	switch (result)
	{
		case VK_SUCCESS:
			return "Command successfully completed";
		case VK_NOT_READY:
			return "A fence or query has not yet completed";
		case VK_TIMEOUT:
			return "A wait operation has not completed in the specified time";
		case VK_EVENT_SET:
			return "An event is signaled";
		case VK_EVENT_RESET:
			return "An event is unsignaled";
		case VK_INCOMPLETE:
			return "A return array was too small for the result";
		case VK_SUBOPTIMAL_KHR:
			return "A swapchain no longer matches the surface properties exactly, but can still be used to present to the surface successfully.";
		case VK_ERROR_OUT_OF_HOST_MEMORY:
			return "A host memory allocation has failed.";
		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			return "A device memory allocation has failed.";
		case VK_ERROR_INITIALIZATION_FAILED:
			return "Initialization of an object could not be completed for implementation-specific reasons.";
		case VK_ERROR_DEVICE_LOST:
			return "The logical or physical device has been lost.";
		case VK_ERROR_MEMORY_MAP_FAILED:
			return "Mapping of a memory object has failed.";
		case VK_ERROR_LAYER_NOT_PRESENT:
			return "A requested layer is not present or could not be loaded.";
		case VK_ERROR_EXTENSION_NOT_PRESENT:
			return "A requested extension is not supported.";
		case VK_ERROR_FEATURE_NOT_PRESENT:
			return "A requested feature is not supported.";
		case VK_ERROR_INCOMPATIBLE_DRIVER:
			return "The requested version of Vulkan is not supported by the driver or is otherwise incompatible for implementation-specific reasons.";
		case VK_ERROR_TOO_MANY_OBJECTS:
			return "Too many objects of the type have already been created.";
		case VK_ERROR_FORMAT_NOT_SUPPORTED:
			return "A requested format is not supported on this device.";
		case VK_ERROR_FRAGMENTED_POOL:
			return "A pool allocation has failed due to fragmentation of the pool's memory. This must only be returned if no attempt to allocate host or device memory was made to accomodate the new allocation. This should be returned in preference to VK_ERROR_OUT_OF_POOL_MEMORY, but only if the implementation is certain that the pool allocation failure was due to fragmentation.";
		case VK_ERROR_SURFACE_LOST_KHR:
			return "A surface is no longer available.";
		case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
			return "The requested window is already in use by Vulkan or another API in a manner which prevents it from being used again.";
		case VK_ERROR_OUT_OF_DATE_KHR:
			return "A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail. Applications must query the new surface properties and recreate their swapchain if they wish to continue presenting to the surface.";
		case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
			return "The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an image.";
		case VK_ERROR_INVALID_SHADER_NV:
			return "One or more shaders failed to compile or link. More details are reported back to the application via ../../html/vkspec.html#VK_EXT_debug_report if enabled.";
		case VK_ERROR_OUT_OF_POOL_MEMORY:
			return "A pool memory allocation has failed. This must only be returned if no attempt to allocate host or device memory was made to accomodate the new allocation. If the failure was definitely due to fragmentation of the pool, VK_ERROR_FRAGMENTED_POOL should be returned instead.";
		case VK_ERROR_INVALID_EXTERNAL_HANDLE:
			return "An external handle is not a valid handle of the specified type.";
		case VK_ERROR_FRAGMENTATION_EXT:
			return "A descriptor pool creation has failed due to fragmentation.";
		default:
			return "UNEXPECTED VK_RESULT CODE.";
		break;
	}
	return "UNEXPECTED VK_RESULT CODE.";
}

const char* vkuGetPhysicalDeviceTypeSignification(const VkPhysicalDeviceType physicalDeviceType)
{
	switch (physicalDeviceType)
	{
		case VK_PHYSICAL_DEVICE_TYPE_OTHER:
			return "VK_PHYSICAL_DEVICE_TYPE_OTHER : The device does not match any other available types.";
		case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
			return "VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU : The device is typically one embedded in or tightly coupled with the host.";
		case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
			return "VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU : The device is typically a separate processor connected to the host via an interlink.";
		case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
			return "VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU : The device is typically a virtual node in a virtualization environment.";
		case VK_PHYSICAL_DEVICE_TYPE_CPU:
			return "VK_PHYSICAL_DEVICE_TYPE_CPU : The device is typically running on the same processors as the host.";
		case VK_PHYSICAL_DEVICE_TYPE_RANGE_SIZE:
			return "Internal definition: VK_PHYSICAL_DEVICE_TYPE_RANGE_SIZE = (VK_PHYSICAL_DEVICE_TYPE_CPU - VK_PHYSICAL_DEVICE_TYPE_OTHER + 1)";
		case VK_PHYSICAL_DEVICE_TYPE_MAX_ENUM:
			return "Internal definition: VK_PHYSICAL_DEVICE_TYPE_MAX_ENUM = 0x7FFFFFFF";
	}
	return "UNEXPECTED VK_PHYSICAL_DEVICE_TYPE CODE.";
}

const char* vkuGetMemoryPropertyFlagBitsSignification(const VkMemoryPropertyFlagBits memoryPropertyFlagBits)
{
	switch (memoryPropertyFlagBits)
	{
		case VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT:
			return "VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT : Indicates that memory allocated with this type is the most efficient for device access. This property will be set if and only if the memory type belongs to a heap with the VK_MEMORY_HEAP_DEVICE_LOCAL_BIT set.";
		case VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT:
			return "VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT : Indicates that memory allocated with this type can be mapped for host access using vkMapMemory.";
		case VK_MEMORY_PROPERTY_HOST_COHERENT_BIT:
			return "VK_MEMORY_PROPERTY_HOST_COHERENT_BIT : Indicates that the host cache management commands vkFlushMappedMemoryRanges and vkInvalidateMappedMemoryRanges are not needed to flush host writes to the device or make device writes visible to the host, respectively.";
		case VK_MEMORY_PROPERTY_HOST_CACHED_BIT:
			return "VK_MEMORY_PROPERTY_HOST_CACHED_BIT : Indicates that memory allocated with this type is cached on the host. Host memory accesses to uncached memory are slower than to cached memory, however uncached memory is always host coherent.";
		case VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT:
			return "VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT : Indicates that the memory type only allows device access to the memory. Memory types must not have both VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT and VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT set. Additionally, the object's backing memory may be provided by the implementation lazily as specified in Lazily Allocated Memory.";
		case VK_MEMORY_PROPERTY_PROTECTED_BIT:
			return "VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT : Indicates that the memory type only allows device access to the memory, and allows protected queue operations to access the memory. Memory types must not have VK_MEMORY_PROPERTY_PROTECTED_BIT set and any of VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT set, or VK_MEMORY_PROPERTY_HOST_COHERENT_BIT set, or VK_MEMORY_PROPERTY_HOST_CACHED_BIT set.";
		case VK_MEMORY_PROPERTY_FLAG_BITS_MAX_ENUM:
			return "Internal definition: VK_MEMORY_PROPERTY_FLAG_BITS_MAX_ENUM = 0x7FFFFFFF";
	}
	return "UNEXPECTED VK_MEMORY_PROPERTY_FLAG_BITS CODE.";
}

const char* vkuGetMemoryHeapFlagBitsSignification(const VkMemoryHeapFlagBits memoryHeapFlagBits)
{
	switch (memoryHeapFlagBits)
	{
		case VK_MEMORY_HEAP_DEVICE_LOCAL_BIT:
			return "VK_MEMORY_HEAP_DEVICE_LOCAL_BIT : Indicates that the heap corresponds to device local memory. Device local memory may have different performance characteristics than host local memory, and may support different memory property flags.";
		case VK_MEMORY_HEAP_MULTI_INSTANCE_BIT:
			return "VK_MEMORY_HEAP_DEVICE_LOCAL_BIT : Indicates that in a logical device representing more than one physical device, there is a per-physical device instance of the heap memory. By default, an allocation from such a heap will be replicated to each physical device's instance of the heap.";
		case VK_MEMORY_HEAP_FLAG_BITS_MAX_ENUM:
			return "Internal definition: VK_MEMORY_HEAP_FLAG_BITS_MAX_ENUM = 0x7FFFFFFF";
	}
	return "UNEXPECTED VK_MEMORY_HEAP_FLAG_BITS CODE.";
}

const char* vkuGetSystemAllocationScopeSignification(const VkSystemAllocationScope systemAllocationScope)
{
	switch (systemAllocationScope)
	{
		case VK_SYSTEM_ALLOCATION_SCOPE_COMMAND:
			return "VK_SYSTEM_ALLOCATION_SCOPE_COMMAND : Specifies that the allocation is scoped to the duration of the Vulkan command.";
		case VK_SYSTEM_ALLOCATION_SCOPE_OBJECT:
			return "VK_SYSTEM_ALLOCATION_SCOPE_OBJECT : Specifies that the allocation is scoped to the lifetime of the Vulkan object that is being created or used.";
		case VK_SYSTEM_ALLOCATION_SCOPE_CACHE:
			return "VK_SYSTEM_ALLOCATION_SCOPE_CACHE : Specifies that the allocation is scoped to the lifetime of a VkPipelineCache or VkValidationCacheEXT object.";
		case VK_SYSTEM_ALLOCATION_SCOPE_DEVICE:
			return "VK_SYSTEM_ALLOCATION_SCOPE_DEVICE : Specifies that the allocation is scoped to the lifetime of the Vulkan device.";
		case VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE:
			return "VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE : Specifies that the allocation is scoped to the lifetime of the Vulkan instance.";
		case VK_SYSTEM_ALLOCATION_SCOPE_RANGE_SIZE:
			return "VK_SYSTEM_ALLOCATION_SCOPE_RANGE_SIZE = (VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE - VK_SYSTEM_ALLOCATION_SCOPE_COMMAND + 1)";
		case VK_SYSTEM_ALLOCATION_SCOPE_MAX_ENUM:
			return "VK_SYSTEM_ALLOCATION_SCOPE_MAX_ENUM = 0x7FFFFFFF";
	}
	return "UNEXPECTED VK_MEMORY_HEAP_FLAG_BITS CODE.";
}

const char* vkuGetInternalAllocationTypeSignification(const VkInternalAllocationType internalAllocationType)
{
	switch (internalAllocationType)
	{
		case VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE:
			return "VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE : Specifies that the allocation is intended for execution by the host.";
		case VK_INTERNAL_ALLOCATION_TYPE_RANGE_SIZE:
			return "VK_INTERNAL_ALLOCATION_TYPE_RANGE_SIZE = (VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE - VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE + 1)";
		case VK_INTERNAL_ALLOCATION_TYPE_MAX_ENUM:
			return " VK_INTERNAL_ALLOCATION_TYPE_MAX_ENUM = 0x7FFFFFFF";
	}
	return "UNEXPECTED VK_MEMORY_HEAP_FLAG_BITS CODE.";
}
