/********************************/
// Name        : main.cpp       */
// Author      : Arthur Covanov */
/* Version     : 0.1            */
/* Copyright   : GPL            */
// Description :                */
/********************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <vulkan/vulkan.h>

#include "vulkan_utils.h"

#define VULKAN_UTILS_MAKE_VERSION(major, minor, patch) (((major) << 22) | ((minor) << 12) | (patch))

// initialize the VkApplicationInfo structure
VkApplicationInfo initVkApplicationInfo()
{
	VkApplicationInfo applicationInfo;
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pNext = NULL;
	applicationInfo.pApplicationName = "Vulkan_Utils_Test";
	applicationInfo.applicationVersion = 1;
	applicationInfo.pEngineName = "Vulkan_Utils";
	applicationInfo.engineVersion = VULKAN_UTILS_MAKE_VERSION(1, 0, 0);
	//applicationInfo.apiVersion = VK_MAKE_VERSION(1, 0, 61); //VK_API_VERSION_1_0; //VK_MAKE_VERSION(1, 0, 0);
	applicationInfo.apiVersion = VK_MAKE_VERSION(1, 1, 97); //VK_API_VERSION_1_0; //VK_MAKE_VERSION(1, 0, 0);
	return applicationInfo;
}

// initialize the VkInstanceCreateInfo structure
VkInstanceCreateInfo initVkInstanceCreateInfo(const VkApplicationInfo *applicationInfo)
{
	VkInstanceCreateInfo instanceCreateInfo;
	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCreateInfo.pNext = NULL;
	instanceCreateInfo.flags = 0;
	instanceCreateInfo.pApplicationInfo = applicationInfo;
	instanceCreateInfo.enabledExtensionCount = 0;
	instanceCreateInfo.ppEnabledExtensionNames = NULL;
	instanceCreateInfo.enabledLayerCount = 0;
	instanceCreateInfo.ppEnabledLayerNames = NULL;
	return instanceCreateInfo;
}

void enumerateInstanceLayerProperties()
{
	VkResult result; //<- Will contains results of Vulkan functions

	uint32_t layerPropertiesCount = 0;
	VkLayerProperties *layerProperties = NULL;
	result = vkuGetInstanceLayerProperties(&layerPropertiesCount, &layerProperties);
	if(result != VK_SUCCESS)
	{
		fprintf(stderr, "VULKAN ERROR(%i): %s\n", result, vkuGetResultSignification(result));
		return;
		//return EXIT_FAILURE;
	}
	fprintf(stdout, "%i Instance Layer Properties found:\n", layerPropertiesCount);

//	//	vkuListLayerPropertiesNames(layerPropertiesCount, layerProperties);
//	char *layerPropertiesNames;
//	vkuListLayerPropertiesNamesIntoString(layerPropertiesCount, layerProperties, &layerPropertiesNames);
//
//	printf("# %i\n", strlen(layerPropertiesNames));
//	printf("%s", layerPropertiesNames);
//	free(layerPropertiesNames);

	//	vkuDescribeLayerProperties("instance", layerPropertiesCount, layerProperties);
	char *layerPropertiesDescription = NULL;
	int layerPropertiesDescriptionSize = -1;
	layerPropertiesDescriptionSize = vkuDescribeLayerPropertiesIntoString("instance", layerPropertiesCount, layerProperties, &layerPropertiesDescription);
	if(layerPropertiesDescriptionSize > 0)
	{
		//printf("# %i\n", strlen(layerPropertiesDescription));
		fprintf(stdout, "%s", layerPropertiesDescription);
		free(layerPropertiesDescription);
	}

	free(layerProperties);
}

void enumerateInstanceExtensionProperties()
{
	VkResult result; //<- Will contains results of Vulkan functions

	uint32_t extensionPropertiesCount = 0;
	VkExtensionProperties *extensionProperties = NULL;
	result = vkuGetInstanceExtensionProperties(&extensionPropertiesCount, &extensionProperties);
	if(result != VK_SUCCESS)
	{
		fprintf(stderr, "VULKAN ERROR(%i): %s\n", result, vkuGetResultSignification(result));
		return;
		//return EXIT_FAILURE;
	}
	fprintf(stdout, "%i Instance Extension Properties found:\n", extensionPropertiesCount);

	char *extensionPropertiesDescription=NULL;
	int extensionPropertiesDescriptionSize = -1;
	extensionPropertiesDescriptionSize = vkuDescribeExtensionPropertiesIntoString("instance", extensionPropertiesCount, extensionProperties, &extensionPropertiesDescription);
	if(extensionPropertiesDescriptionSize > 0)
	{
		//printf("# %i\n", strlen(extensionPropertiesDescription));
		fprintf(stdout, "%s", extensionPropertiesDescription);
		free(extensionPropertiesDescription);
	}

//	vkuListLayerPropertiesNames(layerPropertiesCount, layerProperties);
	free(extensionProperties);
}

void enumeratePhysicalDevicesProperties(const uint32_t physicalDevicesCount, const VkPhysicalDevice *physicalDevices)
{
	if(physicalDevicesCount<=0 || physicalDevices == NULL)
		return;

	fprintf(stdout, "%i Physical Devices found:\n", physicalDevicesCount);

	//vkuDescribePhysicalDevices(physicalDevicesCount, physicalDevices);
	char *physicalDevicesDescription;
	int physicalDevicesDescriptionSize = 0;
	physicalDevicesDescriptionSize = vkuDescribePhysicalDevicesIntoString(physicalDevicesCount, physicalDevices, &physicalDevicesDescription);
	if(physicalDevicesDescriptionSize > 0)
	{
		//printf("# %lu\n", strlen(physicalDevicesDescription));
		fprintf(stdout, "%s", physicalDevicesDescription);
		free(physicalDevicesDescription);
	}

	//vkuListPhysicalDevicesNames(physicalDevicesCount, physicalDevices);
	/*char *physicalDevicesNames;
	vkuListPhysicalDevicesNamesIntoString(physicalDevicesCount, physicalDevices, &physicalDevicesNames);
	printf("%s", physicalDevicesNames);
	free(physicalDevicesNames);*/
}

int main(int argc, char *argv[])
{
	enumerateInstanceLayerProperties();
	enumerateInstanceExtensionProperties();

	VkResult result; //<- Will contains results of Vulkan functions

	const VkApplicationInfo vkApplicationInfo = initVkApplicationInfo();
	const VkInstanceCreateInfo vkInstanceCreateInfo = initVkInstanceCreateInfo(&vkApplicationInfo);	// initialize the VkInstanceCreateInfo structure
	VkInstance instance;

	VkAllocationCallbacks vkuAllocator = vkuGenVkAllocationCallback();

	result = vkCreateInstance(&vkInstanceCreateInfo, &vkuAllocator, &instance); // <- undefined Reference here
	if(result != VK_SUCCESS)
	{
		fprintf(stderr, "VULKAN ERROR(%i): %s\n", result, vkuGetResultSignification(result));
		return EXIT_FAILURE;
	}

	// ENUMERATE PHYSICAL DEVICES
	uint32_t physicalDevicesCount = 0;
	VkPhysicalDevice *physicalDevices = NULL;
	result = vkuGetPhysicalDevices(&instance, &physicalDevicesCount, &physicalDevices);
	if(result != VK_SUCCESS)
	{
		fprintf(stderr, "VULKAN ERROR(%i): %s\n", result, vkuGetResultSignification(result));
		vkDestroyInstance(instance, NULL);				//Destroy Vulkan Instance
		return EXIT_FAILURE;
	}

	enumeratePhysicalDevicesProperties(physicalDevicesCount, physicalDevices);

	free(physicalDevices);

	vkDestroyInstance(instance, NULL);
	return EXIT_SUCCESS;
}

