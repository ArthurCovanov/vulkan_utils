/***********************************************************************/
/* Name        : vulkan_utils_allocator.c                              */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#include <vulkan/vulkan.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "c_utils.h"
#include "vulkan_utils.h"

VkAllocationCallbacks vkuGenVkAllocationCallback()
{
	VkAllocationCallbacks result;
	result.pUserData = NULL;
	result.pfnAllocation = &vkuAllocationFunction;
	result.pfnReallocation = &vkuReallocationFunction;
	result.pfnFree = &vkuFreeFunction;
	result.pfnInternalAllocation = &vkuInternalAllocationNotification;
	result.pfnInternalFree = &vkuInternalFreeNotification;

	return result;
}

void* VKAPI_CALL vkuAllocationFunction(void* pUserData, size_t size, size_t alignment, VkSystemAllocationScope allocationScope)
{
	fprintf(stdout, "[MEM] - %"PRINT_SIZE_T", %"PRINT_SIZE_T", %s\n", size, alignment, vkuGetSystemAllocationScopeSignification(allocationScope));
	return _aligned_malloc(size, alignment);
}

void* VKAPI_CALL vkuReallocationFunction(void* pUserData, void* pOriginal, size_t size, size_t alignment,  VkSystemAllocationScope allocationScope)
{
	fprintf(stdout, "[MEM] - %"PRINT_SIZE_T", %"PRINT_SIZE_T", %s\n", size, alignment, vkuGetSystemAllocationScopeSignification(allocationScope));
	return _aligned_realloc(pOriginal, size, alignment);
}

void VKAPI_CALL vkuFreeFunction(void* pUserData, void* pMemory)
{
	fprintf(stdout, "[MEM] - freed\n");
	_aligned_free(pMemory);
}

void VKAPI_CALL vkuInternalAllocationNotification(void* pUserData, size_t size, VkInternalAllocationType allocationType, VkSystemAllocationScope allocationScope)
{
	fprintf(stdout, "[MEM-Notification] - InternaAllocation - %"PRINT_SIZE_T", %s, %s\n", size, vkuGetInternalAllocationTypeSignification(allocationType), vkuGetSystemAllocationScopeSignification(allocationScope));
}

void VKAPI_CALL vkuInternalFreeNotification(void* pUserData, size_t size, VkInternalAllocationType allocationType, VkSystemAllocationScope allocationScope)
{
	fprintf(stdout, "[MEM-Notification] - InternaFree - %"PRINT_SIZE_T", %s, %s\n", size, vkuGetInternalAllocationTypeSignification(allocationType), vkuGetSystemAllocationScopeSignification(allocationScope));
}
