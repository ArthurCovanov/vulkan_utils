/***********************************************************************/
/* Name        : vulkan_utils_allocator.h                              */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#ifndef INC_VULKAN_UTILS_ALLOCATOR_H_
#define INC_VULKAN_UTILS_ALLOCATOR_H_

#include <vulkan/vulkan.h>

VkAllocationCallbacks vkuGenVkAllocationCallback();

void* VKAPI_CALL vkuAllocationFunction(void* pUserData, size_t size, size_t alignment, VkSystemAllocationScope allocationScope);
void* VKAPI_CALL vkuReallocationFunction(void* pUserData, void* pOriginal, size_t size, size_t alignment,  VkSystemAllocationScope allocationScope);
void VKAPI_CALL vkuFreeFunction(void* pUserData, void* pMemory);
void VKAPI_CALL vkuInternalAllocationNotification(void* pUserData, size_t size, VkInternalAllocationType allocationType, VkSystemAllocationScope allocationScope);
void VKAPI_CALL vkuInternalFreeNotification(void* pUserData, size_t size, VkInternalAllocationType allocationType, VkSystemAllocationScope allocationScope);


#endif /* INC_VULKAN_UTILS_ALLOCATOR_H_ */
