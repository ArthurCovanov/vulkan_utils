/***********************************************************************/
/* Name        : vulkan_utils_descriptions_into_string.h               */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#ifndef INC_VULKAN_UTILS_DESCRIPTIONS_INTO_STRING_H_
#define INC_VULKAN_UTILS_DESCRIPTIONS_INTO_STRING_H_

#include <vulkan/vulkan.h>

int vkuDescribeSampleCountFlagsIntoString(const VkSampleCountFlags sampleCountFlags, char **target);
int vkuDescribeMemoryPropertyFlagsIntoString(const VkMemoryPropertyFlags memoryPropertyFlags, char **target);
int vkuDescribeQueueFlagsIntoString(const VkQueueFlags queueFlags, char **target);
int vkuDescribeMemoryHeapFlagsIntoString(const VkMemoryHeapFlags memoryHeapFlags, char **target);
int vkuDescribePhysicalDeviceLimitsPropertiesIntoString(const char *physicalDeviceLimitsName, const VkPhysicalDeviceLimits physicalDeviceLimits, char **target);
int vkuDescribePhysicalDeviceSparsePropertiesIntoString(const char *physicalDeviceSparsePropertiesName, const VkPhysicalDeviceSparseProperties physicalDeviceSparseProperties, char **target);
int vkuDescribePhysicalDevicePropertiesIntoString(const char *physicalDevicePropertiesName, const VkPhysicalDeviceProperties physicalDeviceProperties, char **target);
int vkuDescribePhysicalDeviceFeaturesIntoString(const char *physicalDeviceFeaturesName, const VkPhysicalDeviceFeatures physicalDeviceFeatures, char **target);
int vkuDescribeMemoryTypeIntoString(const char *memoryTypeName, const VkMemoryType memoryType, char **target);
int vkuDescribeMemoryHeapIntoString(const char *memoryHeapName, const VkMemoryHeap memoryHeap, char **target);
int vkuDescribePhysicalDeviceMemoryPropertiesIntoString(const char *physicalDeviceMemoryPropertiesName, const VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties, char **target);
int vkuDescribeExtent3DIntoString(const char *extent3DName, VkExtent3D extent3D, char **target);
int vkuDescribeQueueFamilyPropertiesIntoString(const char *queueFamilyPropertiesName, const VkQueueFamilyProperties queueFamilyProperties, char **target);
int vkuDescribeQueuesFamilyPropertiesIntoString(const char *physicalDeviceName, const uint32_t queueFamilyPropertiesCount, const VkQueueFamilyProperties *queueFamilyProperties, char **target);
int vkuDescribePhysicalDeviceIntoString(const char *physicalDeviceName, const VkPhysicalDevice physicalDevice, char **target);
int vkuDescribePhysicalDevicesIntoString(const uint32_t physicalDevicesCount, const VkPhysicalDevice *physicalDevices, char **target);
int vkuListPhysicalDevicesNamesIntoString(const uint32_t physicalDevicesCount, const VkPhysicalDevice *physicalDevices, char **target);
int vkuDescribeLayerPropertiesIntoString(const char *physicalDeviceName, const uint32_t layerPropertiesCount, const VkLayerProperties *layerProperties, char **target);
int vkuDescribeExtensionPropertiesIntoString(const char *physicalDeviceName, const uint32_t extensionPropertiesCount, const VkExtensionProperties *extensionProperties, char **target);
int vkuListLayerPropertiesNamesIntoString(const uint32_t layerPropertiesCount, const VkLayerProperties *layerProperties, char **target);

#endif /* INC_VULKAN_UTILS_DESCRIPTIONS_INTO_STRING_H_ */
