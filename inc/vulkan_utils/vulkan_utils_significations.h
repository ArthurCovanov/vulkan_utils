/***********************************************************************/
/* Name        : vulkan_utils_significations.h                         */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#ifndef INC_VULKAN_UTILS_SIGNIFICATIONS_H_
#define INC_VULKAN_UTILS_SIGNIFICATIONS_H_

#include <vulkan/vulkan.h>

const char* vkuGetResultSignification(const VkResult result);
const char* vkuGetPhysicalDeviceTypeSignification(const VkPhysicalDeviceType physicalDeviceType);
const char* vkuGetMemoryPropertyFlagBitsSignification(const VkMemoryPropertyFlagBits memoryPropertyFlagBits);
const char* vkuGetMemoryHeapFlagBitsSignification(const VkMemoryHeapFlagBits memoryHeapFlagBits);

const char* vkuGetSystemAllocationScopeSignification(const VkSystemAllocationScope systemAllocationScope);
const char* vkuGetInternalAllocationTypeSignification(const VkInternalAllocationType internalAllocationType);

#endif /* INC_VULKAN_UTILS_SIGNIFICATIONS_H_ */
