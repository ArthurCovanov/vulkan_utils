/***********************************************************************/
/* Name        : vulkan_utils_descriptions.h                           */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#ifndef INC_VULKAN_UTILS_DESCRIPTIONS_H_
#define INC_VULKAN_UTILS_DESCRIPTIONS_H_

#include "vulkan_utils/vulkan_utils_descriptions_into_string.h"

void vkuDescribeSampleCountFlags(const VkSampleCountFlags sampleCountFlags);
void vkuDescribeMemoryPropertyFlags(const VkMemoryPropertyFlags memoryPropertyFlags);
void vkuDescribeQueueFlags(const VkQueueFlags queueFlags);
void vkuDescribeMemoryHeapFlags(const VkMemoryHeapFlags memoryHeapFlags);
void vkuDescribePhysicalDeviceLimitsProperties(const char *physicalDeviceLimitsName, const VkPhysicalDeviceLimits physicalDeviceLimits);
void vkuDescribePhysicalDeviceSparseProperties(const char *physicalDeviceSparsePropertiesName, const VkPhysicalDeviceSparseProperties physicalDeviceSparseProperties);
void vkuDescribePhysicalDeviceProperties(const char *physicalDevicePropertiesName, const VkPhysicalDeviceProperties physicalDeviceProperties);
void vkuDescribePhysicalDeviceFeatures(const char *physicalDeviceFeaturesName, const VkPhysicalDeviceFeatures physicalDeviceFeatures);
void vkuDescribeMemoryType(const char *memoryTypeName, const VkMemoryType memoryType);
void vkuDescribeMemoryHeap(const char *memoryHeapName, const VkMemoryHeap memoryHeap);
void vkuDescribePhysicalDeviceMemoryProperties(const char *physicalDeviceMemoryPropertiesName, const VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties);
void vkuDescribeExtent3D(const char *extent3DName, VkExtent3D extent3D);
void vkuDescribeQueueFamilyProperties(const char *queueFamilyPropertiesName, const VkQueueFamilyProperties queueFamilyProperties);
void vkuDescribeQueuesFamilyProperties(const char *physicalDeviceName, const uint32_t queueFamilyPropertiesCount, const VkQueueFamilyProperties *queueFamilyProperties);
void vkuDescribePhysicalDevice(const char *physicalDeviceName, const VkPhysicalDevice physicalDevice);
void vkuDescribePhysicalDevices(const uint32_t physicalDevicesCount, const VkPhysicalDevice *physicalDevices);
void vkuListPhysicalDevicesNames(const uint32_t physicalDevicesCount, const VkPhysicalDevice *physicalDevices);
void vkuDescribeLayerProperties(const char *physicalDeviceName, const uint32_t layerPropertiesCount, const VkLayerProperties *layerProperties);
void vkuDescribeExtensionProperties(const char *physicalDeviceName, const uint32_t extensionPropertiesCount, const VkExtensionProperties *extensionProperties);
void vkuListLayerPropertiesNames(const uint32_t layerPropertiesCount, const VkLayerProperties *layerProperties);

#endif /* INC_VULKAN_UTILS_DESCRIPTIONS_H_ */
