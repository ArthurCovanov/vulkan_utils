/***********************************************************************/
/* Name        : vulkan_utils_getters.h                                */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#ifndef INC_VULKAN_UTILS_GETTERS_H_
#define INC_VULKAN_UTILS_GETTERS_H_

#include <vulkan/vulkan.h>

void vkuGetQueueFamilyProperties(const VkPhysicalDevice *physicalDevice, uint32_t *queueFamilyPropertiesCount, VkQueueFamilyProperties **queueFamilyProperties);
VkResult vkuGetPhysicalDevices(const VkInstance *instance, uint32_t *physicalDevicesCount, VkPhysicalDevice **physicalDevices);
VkResult vkuGetDeviceLayerProperties(const VkPhysicalDevice *physicalDevice, uint32_t *layerPropertiesCount, VkLayerProperties **layerProperties);
VkResult vkuGetDeviceExtensionProperties(const VkPhysicalDevice *physicalDevice, uint32_t *extensionPropertiesCount, VkExtensionProperties **extensionProperties);
VkResult vkuGetInstanceLayerProperties(uint32_t *layerPropertiesCount, VkLayerProperties **layerProperties);
VkResult vkuGetInstanceExtensionProperties(uint32_t *extensionPropertiesCount, VkExtensionProperties **extensionProperties);

#endif /* INC_VULKAN_UTILS_GETTERS_H_ */
