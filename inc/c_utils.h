/*************************************/
/* Name        : c_utils.h           */
/* Author      : Arthur Covanov      */
/* Version     : 0.1                 */
/* Copyright   : GPL                 */
/* Description : Usefull tools for C */
/*************************************/

#ifndef INC_C_UTILS_H_
#define INC_C_UTILS_H_

#ifdef _WIN32
#	ifdef _WIN64
#		define PRINT_SIZE_T PRIu64
#	else
#		define PRINT_SIZE_T PRIu32
#	endif
#else
#	define PRINT_SIZE_T "zu"
#endif

#ifdef __cplusplus
extern "C"
{
#endif
// - extern "C" is a linkage-specification
// - Every compiler is required to provide "C" linkage
// - a linkage specification shall occur only in namespace scope
// - all function types, function names and variable names have a language linkage See Richard's Comment: Only function names and variable names with external linkage have a language linkage
// - two function types with distinct language linkages are distinct types even if otherwise identical
// - linkage specs nest, inner one determines the final linkage
// - extern "C" is ignored for class members
// - at most one function with a particular name can have "C" linkage (regardless of namespace)
// - See Richard's comment: 'static' inside 'extern "C"' is valid; an entity so declared has internal linkage, and so does not have a language linkage
// - Linkage from C++ to objects defined in other languages and to objects defined in C++ from other languages is implementation-defined and language-dependent. Only where the object layout strategies of two language implementations are similar enough can such linkage be achieved*/

	//int __cdecl stprintf(char ** __restrict__ _Dest,const char * __restrict__ _Format,...) __MINGW_ATTRIB_DEPRECATED_SEC_WARN;
	int stprintf(char ** __restrict__ _Dest, const char * __restrict__ _Format, ...);

#ifdef __cplusplus
}
#endif

#endif /* INC_C_UTILS_H_ */
