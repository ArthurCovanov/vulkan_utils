/***********************************************************************/
/* Name        : vulkan_utils.h                                        */
/* Author      : Arthur Covanov                                        */
/* Version     : 0.1                                                   */
/* Copyright   : GPL                                                   */
/* Description : Vulkan Utilitaries compatible with Vulkan >= 1.1.73.0 */
/***********************************************************************/

#ifndef INC_VULKAN_UTILS_H_
#define INC_VULKAN_UTILS_H_

// For C++ Linkage
// extern "C" makes a function-name in C++ have 'C' linkage (compiler does not mangle the name) so that client C code can link to (i.e use) your function using a 'C' compatible header file that contains just the declaration of your function. Your function definition is contained in a binary format (that was compiled by your C++ compiler) that the client 'C' linker will then link to using the 'C' name.
// 
// Since C++ has overloading of function names and C does not, the C++ compiler cannot just use the function name as a unique id to link to, so it mangles the name by adding information about the arguments. A C compiler does not need to mangle the name since you can not overload function names in C. When you state that a function has extern "C" linkage in C++, the C++ compiler does not add argument/parameter type // information to the name used for linkage.
// 
// Just so you know, you can specify "C" linkage to each individual declaration/definition explicitly or use a block to group a sequence of declarations/definitions to have a certain linkage:
// 
// If you care about the technicalities, they are listed in section 7.5 of the C++03 standard, here is a brief summary (with emphasis on extern "C"):*

#ifdef __cplusplus
extern "C"
{
#endif

// - extern "C" is a linkage-specification
// - Every compiler is required to provide "C" linkage
// - a linkage specification shall occur only in namespace scope
// - all function types, function names and variable names have a language linkage See Richard's Comment: Only function names and variable names with external linkage have a language linkage
// - two function types with distinct language linkages are distinct types even if otherwise identical
// - linkage specs nest, inner one determines the final linkage
// - extern "C" is ignored for class members
// - at most one function with a particular name can have "C" linkage (regardless of namespace)
// - See Richard's comment: 'static' inside 'extern "C"' is valid; an entity so declared has internal linkage, and so does not have a language linkage
// - Linkage from C++ to objects defined in other languages and to objects defined in C++ from other languages is implementation-defined and language-dependent. Only where the object layout strategies of two language implementations are similar enough can such linkage be achieved*/

#include <vulkan/vulkan.h>

#include "vulkan_utils/vulkan_utils_descriptions.h"
#include "vulkan_utils/vulkan_utils_getters.h"
#include "vulkan_utils/vulkan_utils_significations.h"

#include "vulkan_utils/vulkan_utils_allocator.h"

void vkuVersion();

#ifdef __cplusplus
}
#endif

#endif /* INC_VULKAN_UTILS_H_ */

