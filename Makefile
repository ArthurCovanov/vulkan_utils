CFLAGS = -O2 -g -m64 -Wall -fmessage-length=0

PKGCCFLAGS = `pkg-config --cflags vulkan`
PKGCLIBS   = `pkg-config --libs vulkan`

CMP = gcc

SDIR	= src
IDIR	= inc
LDIR	= lib
ODIR	= obj
BDIR	= bld

OBJS	= vulkan_utils.o vulkan_utils_descriptions_into_string.o vulkan_utils_descriptions.o vulkan_utils_getters.o vulkan_utils_significations.o vulkan_utils_allocator.o

TARGET	= vulkan_utils

TEST_DESCRIPTIONS = bin/test_descriptions
TEST_ALLOCATOR    = bin/test_allocator

descriptions.o: $(SDIR)/descriptions.c
	mkdir -p $(ODIR)
	$(CMP) $(CFLAGS) -o $(ODIR)/$@ -c $^ -I $(IDIR) $(PKGCFLAGS)

allocator.o: $(SDIR)/allocator.c
	mkdir -p $(ODIR)
	$(CMP) $(CFLAGS) -o $(ODIR)/$@ -c $^ -I $(IDIR) $(PKGCFLAGS)

#c_utils.o: $(SDIR)/c_utils.c
#	mkdir -p $(ODIR)
#	$(CMP) $(CFLAGS) -o $(ODIR)/$@ -c $^ -I $(IDIR)

vulkan_utils.o: $(SDIR)/vulkan_utils.c
	mkdir -p $(ODIR)
	$(CMP) $(CFLAGS) -o $(ODIR)/$@ -c $^ -I $(IDIR) $(PKGCFLAGS)

vulkan_utils_descriptions_into_string.o: $(SDIR)/vulkan_utils_descriptions_into_string.c
	mkdir -p $(ODIR)
	$(CMP) $(CFLAGS) -o $(ODIR)/$@ -c $^ -I $(IDIR) $(PKGCFLAGS)

vulkan_utils_descriptions.o: $(SDIR)/vulkan_utils_descriptions.c
	mkdir -p $(ODIR)
	$(CMP) $(CFLAGS) -o $(ODIR)/$@ -c $^ -I $(IDIR) $(PKGCFLAGS)

vulkan_utils_getters.o: $(SDIR)/vulkan_utils_getters.c
	mkdir -p $(ODIR)
	$(CMP) $(CFLAGS) -o $(ODIR)/$@ -c $^ -I $(IDIR) $(PKGCFLAGS)

vulkan_utils_significations.o: $(SDIR)/vulkan_utils_significations.c
	mkdir -p $(ODIR)
	$(CMP) $(CFLAGS) -o $(ODIR)/$@ -c $^ -I $(IDIR) $(PKGCFLAGS)

vulkan_utils_allocator.o: $(SDIR)/vulkan_utils_allocator.c
	mkdir -p $(ODIR)
	$(CMP) $(CFLAGS) -o $(ODIR)/$@ -c $^ -I $(IDIR) $(PKGCFLAGS)

$(TARGET): $(OBJS)
	mkdir -p $(LDIR)
	$(AR) rcs $(LDIR)/lib$@.a $(ODIR)/*.o

$(TEST_DESCRIPTIONS): $(TARGET) descriptions.o
	mkdir -p bin
	$(CMP) $(CFLAGS) -o $@ $(ODIR)/descriptions.o -L$(LDIR) -l$(TARGET) -lstdc++ $(PKGCLIBS) -L$(BDIR) -lIO_utils

$(TEST_ALLOCATOR): $(TARGET) allocator.o
	mkdir -p bin
	$(CMP) $(CFLAGS) -o $@ $(ODIR)/allocator.o -L$(LDIR) -l$(TARGET) -lstdc++ $(PKGCLIBS) -L$(BDIR) -lIO_utils

test: $(TEST_DESCRIPTIONS) $(TEST_ALLOCATOR)


all:	clean $(TARGET) test


clean:
	rm -rf $(ODIR) $(LDIR) $(TEST_DESCRIPTIONS) $(TEST_ALLOCATOR)
	mkdir $(ODIR) $(LDIR)

